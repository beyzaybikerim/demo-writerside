# Referenced Topic File

This is a referenced topic file name

## Example chapter {id="some-chapter-id"}

Some text.

### Subchapter {collapsible="true"}

Some more {anchor="some-chapter-id"} [text](http://localhost:63342/writer-side-demo/preview/referenced-topic-file.html).

<include from="Empty.md" element-id="generic-warning"/>

### Another Chaper

some text some text <a anchor="some-chapter-id"/>
