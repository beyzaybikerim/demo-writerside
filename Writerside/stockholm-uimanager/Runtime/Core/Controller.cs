using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MatchinghamGames.StockholmCore.Runtime
{
    /// <summary>
    /// Controllers are supposed to keep action definitions and they are used as a main component in a UserController.
    /// </summary>
    public abstract class Controller : MonoBehaviour
    {
        [NonSerialized] public bool SetToUnmountOnClose = false;

        protected virtual void Awake()
        {
            Initialize();
            AddHandlers();
        }

        /// <summary>
        /// Initialize here
        /// </summary>
        protected abstract void Initialize();

        /// <summary>
        /// Register your actions here
        /// </summary>
        protected abstract void AddHandlers();

        /// <summary>
        /// Unsubscribe your actions here
        /// </summary>
        protected virtual void RemoveHandlers()
        {
        }

        public virtual void ResetController()
        {
            gameObject.SetActive(false);
        }

        public virtual void Open()
        {
            gameObject.SetActive(true);
        }

        public virtual void Close()
        {
            gameObject.SetActive(false);

            if (SetToUnmountOnClose)
            {
                this.Unmount();
            }
        }

        protected virtual void OnDestroy()
        {
            RemoveHandlers();
        }
    }

    /// <summary>
    /// Controllers are supposed to keep action definitions and they are used as a main component in a UserController.
    /// It uses a View attached.
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    public abstract class Controller<TView> : Controller
        where TView : View
    {
#pragma warning disable 649
        [SerializeField] protected TView view;
#pragma warning restore 649

        protected TView View => view;

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (!TryGetComponent(out view))
                view = gameObject.AddComponent<TView>();

            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif

        /// <summary>
        /// Initializes the controller
        /// </summary>
        protected override void Initialize()
        {
            View.Initialize();
        }

        /// <summary>
        /// Resets the view back to the initial condition
        /// </summary>
        public override void ResetController()
        {
            view.ResetView();
        }

        /// <summary>
        /// Opens the view
        /// </summary>
        public override void Open()
        {
            view.Open();
        }

        /// <summary>
        /// Closes the view
        /// </summary>
        public override void Close()
        {
            if (SetToUnmountOnClose)
            {
                if (view.IsOpen)
                {
                    view.OnCloseEnd += this.Unmount;
                }
                else
                {
                    this.Unmount();
                    return;
                }
            }

            view.Close();
        }
    }

    /// <summary>
    /// Controllers are supposed to keep action definitions and they are used as a main component in a UserController.
    /// It uses a View and a Model attached.
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    /// <typeparam name="TModel"></typeparam>
    public abstract class Controller<TView, TModel> : Controller<TView>
        where TView : View<TModel>
        where TModel : BaseModel, new()
    {
        private bool isModelSet;
        private TModel model;

        public TModel Model
        {
            get
            {
                if (!isModelSet)
                {
                    SetModel();
                }

                return model;
            }
            set
            {
                model = value;
            }
        }

        /// <summary>
        /// Initializes the controller
        /// </summary>
        protected override void Initialize()
        {
            if (!isModelSet)
            {
                SetModel();
            }

            base.Initialize();
        }

        private void SetModel()
        {
            model = new TModel();
            View.SetModel(model);
            isModelSet = true;
        }

        /// <summary>
        /// Resets the view back to the initial condition
        /// </summary>
        public override void ResetController()
        {
            if (!isModelSet)
            {
                SetModel();
            }

            view.ResetView();
        }

        /// <summary>
        /// Opens the view
        /// </summary>
        public override void Open()
        {
            if (!isModelSet)
            {
                SetModel();
            }

            view.Open();
        }

        /// <summary>
        /// Closes the view
        /// </summary>
        public override void Close()
        {
            if (!isModelSet)
            {
                SetModel();
            }

            if (SetToUnmountOnClose)
            {
                if (view.IsOpen)
                {
                    view.OnCloseEnd += this.Unmount;
                }
                else
                {
                    this.Unmount();
                    return;
                }
            }

            view.Close();
        }
    }
}