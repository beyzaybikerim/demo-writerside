﻿using System.Collections.Generic;
using UnityEngine;
using Utilities;
// ReSharper disable VirtualMemberCallInConstructor

namespace MatchinghamGames.StockholmCore.Runtime
{
    public class BaseModel
    {
        private bool isBound;
        private Dictionary<string, ReactiveMember<object>> cache;
        
        protected BaseModel()
        {
            cache = new Dictionary<string, ReactiveMember<object>>();
            Initialize();
        }

        protected virtual void Initialize()
        {
            // Initialize filed here
        }

        public object Get(string key)
        {
            return cache[key]?.member.Value;
        }

        public T Get<T>(string key)
        {
            return (T)cache[key]?.member.Value;
        }

        public void Set(string key, object value)
        {
            if (!cache.TryGetValue(key, out var cached))
            {
                cache.Add(key, new ReactiveMember<object>(value));
            }
            else
            {
                cached.member.Value = value;
            }
        }

        public void Bind()
        {
            if (isBound)
            {
                return;
            }
            
            isBound = true;
            
            foreach (var item in cache)
            {
                item.Value?.Bind();
            }
        }

        public void Unbind()
        {
            if (!isBound)
            {
                return;
            }
            
            isBound = false;
            
            foreach (var item in cache)
            {
                item.Value?.Unbind();
            }
        }

        public void Subscribe(string key, Subject<object>.SubscriptionCallback callback)
        {
            if (!cache.TryGetValue(key, out var cached))
            {
                Debug.LogError($"Cannot subscribe, {key} does not exist.");
                return;
            }
            
            cached.Subscribe(callback, isBound);
        }

        public void Unsubscribe(string key, Subject<object>.SubscriptionCallback callback)
        {
            if (!cache.TryGetValue(key, out var cached))
            {
                Debug.LogError($"Cannot unsubscribe, {key} does not exist.");
                return;
            }
            
            cached.Unsubscribe(callback, isBound);
        }
        
        private class ReactiveMember<T>
        {
            public Subject<T> member;
            private readonly List<Subject<T>.SubscriptionCallback> bindings;

            public ReactiveMember(T newValue)
            {
                member = new Subject<T>(newValue);
                bindings = new List<Subject<T>.SubscriptionCallback>();
            }

            public void Bind()
            {
                foreach (var val in bindings)
                {
                    member?.Subscribe(val);
                }
            }
            
            public void Unbind()
            {
                foreach (var val in bindings)
                {
                    member?.Unsubscribe(val);
                }
            }
            
            public void Subscribe(Subject<T>.SubscriptionCallback callback, bool isBound)
            {
                if (callback == null)
                {
                    Debug.LogError($"Callback cannot be null.");
                    return;
                }

                if (isBound)
                {
                    member.Subscribe(callback);    
                }
                
                bindings.Add(callback);
            }
            
            public void Unsubscribe(Subject<T>.SubscriptionCallback callback, bool isBound)
            {
                if (callback == null)
                {
                    Debug.LogError($"Callback cannot be null.");
                    return;
                }

                if (isBound)
                {
                    member.Unsubscribe(callback);    
                }
                
                bindings.Remove(callback);
            }
        }

    }
}