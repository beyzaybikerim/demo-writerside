using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UIElements;

namespace MatchinghamGames.StockholmCore.Runtime
{
    public abstract class View : MonoBehaviour
    {
        public delegate void OnOpenBeginEvent();

        public delegate void OnOpenEndEvent();

        public delegate void OnCloseBeginEvent();

        public delegate void OnCloseEndEvent();

        private bool isBound = false;
        
        [SerializeField, BoxGroup("Base Components")]
        protected bool resetBeforeOpenCall;

        [SerializeField, BoxGroup("Base Components")]
        protected bool keepBindingsOnWhenClosed = false;

        /// <summary>
        /// Event thrown when the Open animation begins
        /// </summary>
        public OnOpenBeginEvent OnOpenBegin;

        /// <summary>
        /// Event thrown when the Open animation ends
        /// </summary>
        public OnOpenEndEvent OnOpenEnd;

        /// <summary>
        /// Event thrown when the Close animation begins
        /// </summary>
        public OnCloseBeginEvent OnCloseBegin;

        /// <summary>
        /// Event thrown when the Close animation ends
        /// </summary>
        public OnCloseEndEvent OnCloseEnd;

        /// <summary>
        /// Returns if open or close animation is playing.
        /// </summary>
        public bool IsOpening { get; set; }

        /// <summary>
        /// Returns if open or close animation is playing.
        /// </summary>
        public bool IsClosing { get; set; }

        /// <summary>
        /// Returns if View fully open.
        /// </summary>
        public bool IsOpen { get; set; }

        public bool IsBound
        {
            get { return isBound; }
            set
            {
                if (keepBindingsOnWhenClosed)
                {
                    return;
                }
                
                if (value != isBound)
                {
                    isBound = value;
                    OnBindingUpdate();
                }
            }
        }

        protected abstract void InitiateOpen();
        protected abstract void InitiateClose();
        protected abstract void InitiateReset();
        protected abstract void AddHandlers();
        protected abstract void RemoveHandlers();

        protected virtual void OnOpenCommandGiven()
        {
        }

        protected virtual void OnCloseCommandGiven()
        {
        }

        protected virtual void OnResetCommandGiven()
        {
        }

        public virtual void Initialize()
        {
            if (keepBindingsOnWhenClosed)
            {
                IsBound = true;
            }
        }

        /// <summary>
        /// Resets the view back to the initial condition
        /// </summary>
        public virtual void ResetView()
        {
            IsOpen = false;
            IsOpening = false;
            IsClosing = false;
            IsBound = false;

            OnResetCommandGiven();
            InitiateReset();
        }

        /// <summary>
        /// Activates the gameobject immediately.
        /// Call Bind when overriding
        /// </summary>
        public virtual void Open()
        {
            if (resetBeforeOpenCall)
            {
                ResetView();
            }
            else
            {
                if (IsOpening)
                {
                    return;
                }
            }

            IsOpening = true;
            IsClosing = false;
            IsBound = true;

            OnOpenCommandGiven();
            InitiateOpen();
        }

        /// <summary>
        /// Deactivates the gameobject immediately.
        /// Call Unbind when overriding
        /// </summary>
        public virtual void Close()
        {
            if (IsClosing)
            {
                return;
            }

            IsOpening = false;
            IsClosing = true;
            IsBound = false;

            OnCloseCommandGiven();
            InitiateClose();
        }

        protected virtual void OnBindingUpdate()
        {
            if (IsBound)
            {
                AddHandlers();
            }
            else
            {
                RemoveHandlers();
            }
        }
    }

    /// <summary>
    /// Views are all about the opening and closing a certain user control or element.
    /// They can be used alone, if methods are accessed someone of course,
    /// yet we encourage it to be used in MVC Framework as described in documentation.
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public abstract class View<TModel> : View
        where TModel : BaseModel
    {
        protected TModel Model { get; private set; }

        internal void SetModel(TModel model)
        {
            Model = model;
        }
        
        protected sealed override void OnBindingUpdate()
        {
            base.OnBindingUpdate();
            
            if (IsBound)
            {
                Model.Bind();
            }
            else
            {
                Model.Unbind();
            }
        }
    }
}