﻿using UnityEngine;

namespace MatchinghamGames.StockholmCore.Runtime
{
    /// <summary>
    /// This behaviour assumes AnimatedView is in hieararchy, i.e. check for parent transform if it cannot find it in transform
    /// </summary>
    public class CloseBehaviour : StateMachineBehaviour
    {
        private IAnimatedView attachedView;
        private bool isComplete;

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!isComplete && stateInfo.normalizedTime >= 1)
            {
                OnStateExit(animator, stateInfo, layerIndex);
            }
        }

        protected bool CheckView(Transform root)
        {
            if (attachedView == null)
            {
                attachedView = root.GetComponent<IAnimatedView>();
            }

            if (attachedView == null)
            {
                if (root.parent != null)
                {
                    return CheckView(root.parent);
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!CheckView(animator.transform))
            {
                isComplete = true;
                return;
            }

            isComplete = false;
            attachedView.OnCloseAnimationStart();
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (isComplete)
            {
                return;
            }

            isComplete = true;
            attachedView.OnCloseAnimationEnd();
        }
    }
}
