﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace MatchinghamGames.StockholmCore.Runtime
{
    public abstract class CurveFadeView : LerpFadeView
    {
        /// <summary>
        /// This should be in between [0,1] in time scale
        /// </summary>
        [SerializeField, BoxGroup("Base Components")] AnimationCurve animationCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        public override float Evaluate(float lerp)
        {
            return animationCurve.Evaluate(lerp);
        }
    }
    
    public abstract class CurveFadeView<TModel> : LerpFadeView<TModel>
        where TModel : BaseModel, new()
    {
        /// <summary>
        /// This should be in between [0,1] in time scale
        /// </summary>
        [SerializeField, BoxGroup("Base Components")] AnimationCurve animationCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        public override float Evaluate(float lerp)
        {
            return animationCurve.Evaluate(lerp);
        }
    }
}
