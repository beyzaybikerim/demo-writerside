namespace MatchinghamGames.StockholmCore.Runtime
{
    public abstract class FadedView : LerpFadeView
    {
        public override float Evaluate(float lerp)
        {
            return lerp;
        }
    }
    
    public abstract class FadedView<TModel> : LerpFadeView<TModel>
        where TModel : BaseModel, new()
    {
        public override float Evaluate(float lerp)
        {
            return lerp;
        }
    }
}
