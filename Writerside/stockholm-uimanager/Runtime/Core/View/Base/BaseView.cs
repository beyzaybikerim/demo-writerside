﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace MatchinghamGames.StockholmCore.Runtime
{
    public abstract class BaseView : View
    {
        [SerializeField, BoxGroup("Base Components")]
        protected GameObject ContentRoot;

        [SerializeField, BoxGroup("Base Components")]
        private GameObject backgroundBlocker;

        [SerializeField, BoxGroup("Base Components")]
        private GameObject foregroundBlocker;

        private View bgBlockerView;
        private View fgBlockerView;

        public bool InputLock
        {
            set
            {
                if (foregroundBlocker != null)
                {
                    if (fgBlockerView != null)
                    {
                        if (value)
                        {
                            fgBlockerView.Open();
                        }
                        else
                        {
                            fgBlockerView.Close();
                        }
                    }
                    else
                    {
                        foregroundBlocker.SetActive(value);
                    }
                }
            }
        }

        public bool BgLock
        {
            set
            {
                if (backgroundBlocker != null)
                {
                    if (bgBlockerView != null)
                    {
                        if (value)
                        {
                            bgBlockerView.Open();
                        }
                        else
                        {
                            bgBlockerView.Close();
                        }
                    }
                    else
                    {
                        backgroundBlocker.SetActive(value);
                    }
                }
            }
        }

        public bool ContentEnabled
        {
            set
            {
                if (ContentRoot != null)
                {
                    ContentRoot.gameObject.SetActive(value);
                }
            }
        }

        protected virtual void Awake()
        {
            if (backgroundBlocker)
            {
                bgBlockerView = backgroundBlocker.GetComponent<View>();
            }

            if (foregroundBlocker)
            {
                fgBlockerView = foregroundBlocker.GetComponent<View>();
            }

            if (fgBlockerView != null)
            {
                foregroundBlocker.SetActive(true);
                foregroundBlocker.GetComponent<Image>().raycastTarget = false;
            }
            
            if (bgBlockerView != null)
            {
                backgroundBlocker.SetActive(true);
                backgroundBlocker.GetComponent<Image>().raycastTarget = false;
            }
        }

        protected virtual void Reset()
        {
            resetBeforeOpenCall = true;
        }

        protected virtual void OnValidate()
        {
            this.ValidateContentRoot();
        }

        [Button]
        private void CreateBgLock()
        {
            var rect = GetComponent<RectTransform>();

            if (rect == null)
            {
                if (!backgroundBlocker)
                {
                    Debug.LogWarning($"{nameof(backgroundBlocker)} is ineffective in non canvas objects.");
                }
            }
            else
            {
                if (backgroundBlocker)
                {
                    return;
                }
                else
                {
                    string inputLockName = "BgOverlay";
                    GameObject newBgLock = this.FindInChildren(inputLockName);

                    if (newBgLock == null)
                    {
                        newBgLock = new GameObject(inputLockName);
                        newBgLock.transform.SetParent(this.transform);
                        newBgLock.transform.localScale = Vector3.one;
                    }

                    if (newBgLock.GetComponent<Image>() == null)
                    {
                        newBgLock.AddComponent<Image>();
                    }

                    backgroundBlocker = newBgLock;
                }

                ValidateBackgroundLock();
            }
        }

        [Button]
        private void CreateInputLock()
        {
            var rect = GetComponent<RectTransform>();

            if (rect == null)
            {
                if (!foregroundBlocker)
                {
                    Debug.LogWarning($"{nameof(foregroundBlocker)} is ineffective in non canvas objects.");
                }
            }
            else
            {
                if (foregroundBlocker)
                {
                    return;
                }
                else
                {
                    string inputLockName = "LockOverlay";
                    GameObject newInputLock = this.FindInChildren(inputLockName);

                    if (newInputLock == null)
                    {
                        newInputLock = new GameObject(inputLockName);
                        newInputLock.transform.SetParent(this.transform);
                        newInputLock.transform.localScale = Vector3.one;
                    }

                    if (newInputLock.GetComponent<Image>() == null)
                    {
                        newInputLock.AddComponent<Image>();
                    }

                    foregroundBlocker = newInputLock;
                }

                ValidateForegroundLock();
            }
        }

        private void ValidateContentRoot()
        {
            if (ContentRoot == null)
            {
                string rootName = "Root";
                GameObject newRoot = this.FindInChildren(rootName);

                if (!newRoot)
                {
                    newRoot = new GameObject(rootName);
                    newRoot.transform.SetParent(transform);
                    newRoot.AddComponent<RectTransform>();
                }

                ContentRoot = newRoot;
                ContentRoot.transform.localPosition = Vector3.zero;
                ContentRoot.transform.localScale = Vector3.one;
            }
        }

        private void ValidateBackgroundLock()
        {
            var rect = GetComponent<RectTransform>();

            if (rect == null)
            {
                if (!backgroundBlocker)
                {
                    Debug.LogWarning($"{nameof(backgroundBlocker)} is ineffective in non canvas objects.");
                }
            }
            else
            {
                if (backgroundBlocker)
                {
                    Image bgLock = backgroundBlocker.GetComponent<Image>();
                    bgLock.SetOverlayFillPanel();

                    backgroundBlocker.transform.SetAsFirstSibling();
                    backgroundBlocker.gameObject.SetActive(false);
                }
            }
        }

        public void ValidateForegroundLock()
        {
            var rect = GetComponent<RectTransform>();

            if (rect == null)
            {
                if (!foregroundBlocker)
                {
                    Debug.LogWarning($"{nameof(foregroundBlocker)} is ineffective in non canvas objects.");
                }
            }
            else
            {
                if (foregroundBlocker)
                {
                    Image inputLock = foregroundBlocker.GetComponent<Image>();
                    inputLock.SetOverlayFillPanel();

                    inputLock.GetComponent<View>();

                    foregroundBlocker.transform.SetAsLastSibling();
                    foregroundBlocker.gameObject.SetActive(false);
                }
            }
        }

        protected void OnOpenComplete()
        {
            if (!IsOpening)
            {
                return;
            }

            IsOpening = false;
            IsOpen = true;
            OnOpenEnd?.Invoke();
        }

        protected void OnCloseComplete()
        {
            if (!IsClosing)
            {
                return;
            }

            IsClosing = false;
            IsOpen = false;
            OnCloseEnd?.Invoke();
        }
    }

    public abstract class BaseView<TModel> : View<TModel>
        where TModel : BaseModel, new()
    {
        [SerializeField, BoxGroup("Base Components")]
        protected GameObject ContentRoot;

        [SerializeField, BoxGroup("Base Components")]
        private GameObject backgroundBlocker;

        [SerializeField, BoxGroup("Base Components")]
        private GameObject foregroundBlocker;

        private View bgBlockerView;
        private View fgBlockerView;

        public bool InputLock
        {
            set
            {
                if (foregroundBlocker != null)
                {
                    if (fgBlockerView != null)
                    {
                        if (value)
                        {
                            fgBlockerView.Open();
                        }
                        else
                        {
                            fgBlockerView.Close();
                        }
                    }
                    else
                    {
                        foregroundBlocker.SetActive(value);
                    }
                }
            }
        }

        public bool BgLock
        {
            set
            {
                if (backgroundBlocker != null)
                {
                    if (bgBlockerView != null)
                    {
                        if (value)
                        {
                            bgBlockerView.Open();
                        }
                        else
                        {
                            bgBlockerView.Close();
                        }
                    }
                    else
                    {
                        backgroundBlocker.SetActive(value);
                    }
                }
            }
        }

        public bool ContentEnabled
        {
            set
            {
                if (ContentRoot != null)
                {
                    ContentRoot.gameObject.SetActive(value);
                }
            }
        }

        protected virtual void Awake()
        {
            if (backgroundBlocker)
            {
                bgBlockerView = backgroundBlocker.GetComponent<View>();
            }

            if (foregroundBlocker)
            {
                fgBlockerView = foregroundBlocker.GetComponent<View>();
            }

            if (fgBlockerView != null)
            {
                foregroundBlocker.SetActive(true);
                foregroundBlocker.GetComponent<Image>().raycastTarget = false;
            }
            
            if (bgBlockerView != null)
            {
                backgroundBlocker.SetActive(true);
                backgroundBlocker.GetComponent<Image>().raycastTarget = false;
            }
        }

        protected virtual void Reset()
        {
            resetBeforeOpenCall = true;
        }

        protected virtual void OnValidate()
        {
            this.ValidateContentRoot();
        }

        [Button]
        private void CreateBgLock()
        {
            var rect = GetComponent<RectTransform>();

            if (rect == null)
            {
                if (!backgroundBlocker)
                {
                    Debug.LogWarning($"{nameof(backgroundBlocker)} is ineffective in non canvas objects.");
                }
            }
            else
            {
                if (backgroundBlocker)
                {
                    return;
                }
                else
                {
                    string inputLockName = "BgOverlay";
                    GameObject newBgLock = this.FindInChildren(inputLockName);

                    if (newBgLock == null)
                    {
                        newBgLock = new GameObject(inputLockName);
                        newBgLock.transform.SetParent(this.transform);
                        newBgLock.transform.localScale = Vector3.one;
                    }

                    if (newBgLock.GetComponent<Image>() == null)
                    {
                        newBgLock.AddComponent<Image>();
                    }

                    backgroundBlocker = newBgLock;
                }

                ValidateBackgroundLock();
            }
        }

        [Button]
        private void CreateInputLock()
        {
            var rect = GetComponent<RectTransform>();

            if (rect == null)
            {
                if (!foregroundBlocker)
                {
                    Debug.LogWarning($"{nameof(foregroundBlocker)} is ineffective in non canvas objects.");
                }
            }
            else
            {
                if (foregroundBlocker)
                {
                    return;
                }
                else
                {
                    string inputLockName = "LockOverlay";
                    GameObject newInputLock = this.FindInChildren(inputLockName);

                    if (newInputLock == null)
                    {
                        newInputLock = new GameObject(inputLockName);
                        newInputLock.transform.SetParent(this.transform);
                        newInputLock.transform.localScale = Vector3.one;
                    }

                    if (newInputLock.GetComponent<Image>() == null)
                    {
                        newInputLock.AddComponent<Image>();
                    }

                    foregroundBlocker = newInputLock;
                }

                ValidateForegroundLock();
            }
        }

        private void ValidateContentRoot()
        {
            if (ContentRoot == null)
            {
                string rootName = "Root";
                GameObject newRoot = this.FindInChildren(rootName);

                if (!newRoot)
                {
                    newRoot = new GameObject(rootName);
                    newRoot.transform.SetParent(transform);
                    newRoot.AddComponent<RectTransform>();
                }

                ContentRoot = newRoot;
                ContentRoot.transform.localPosition = Vector3.zero;
                ContentRoot.transform.localScale = Vector3.one;
            }
        }

        private void ValidateBackgroundLock()
        {
            var rect = GetComponent<RectTransform>();

            if (rect == null)
            {
                if (!backgroundBlocker)
                {
                    Debug.LogWarning($"{nameof(backgroundBlocker)} is ineffective in non canvas objects.");
                }
            }
            else
            {
                if (backgroundBlocker)
                {
                    Image bgLock = backgroundBlocker.GetComponent<Image>();
                    bgLock.SetOverlayFillPanel();

                    backgroundBlocker.transform.SetAsFirstSibling();
                    backgroundBlocker.gameObject.SetActive(false);
                }
            }
        }

        public void ValidateForegroundLock()
        {
            var rect = GetComponent<RectTransform>();

            if (rect == null)
            {
                if (!foregroundBlocker)
                {
                    Debug.LogWarning($"{nameof(foregroundBlocker)} is ineffective in non canvas objects.");
                }
            }
            else
            {
                if (foregroundBlocker)
                {
                    Image inputLock = foregroundBlocker.GetComponent<Image>();
                    inputLock.SetOverlayFillPanel();

                    inputLock.GetComponent<View>();

                    foregroundBlocker.transform.SetAsLastSibling();
                    foregroundBlocker.gameObject.SetActive(false);
                }
            }
        }

        protected void OnOpenComplete()
        {
            if (!IsOpening)
            {
                return;
            }

            IsOpening = false;
            IsOpen = true;
            OnOpenEnd?.Invoke();
        }

        protected void OnCloseComplete()
        {
            if (!IsClosing)
            {
                return;
            }

            IsClosing = false;
            IsOpen = false;
            OnCloseEnd?.Invoke();
        }
    }
}