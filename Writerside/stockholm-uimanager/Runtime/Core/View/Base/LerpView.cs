﻿using System;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace MatchinghamGames.StockholmCore.Runtime
{
    public abstract class LerpView : BaseView
    {
        [SerializeField, BoxGroup("Base Components")]
        private float duration = .5f;

        /// <summary>
        /// Enabling this parameter makes view use Timing Coroutines (see MEC)
        /// </summary>
        [SerializeField, BoxGroup("Base Components")]
        protected bool useMEC = false;

        protected float currentOpenAmount;

        private CoroutineHandle openHandle;
        private CoroutineHandle closeHandle;

        public float Duration
        {
            get { return duration; }
        }

        /// <summary>
        /// Returns a evaluated result for lerp value which is in [0, 1]
        /// </summary>
        /// <param name="lerp"></param>
        /// <returns></returns>
        public abstract float Evaluate(float lerp);

        /// <summary>
        /// Set Animation state evaluating above.
        /// </summary>
        /// <param name="lerp"></param>
        protected abstract void SetOpen(float lerp);

        /// <summary>
        /// Set Animation state evaluating above.
        /// </summary>
        /// <param name="lerp"></param>
        protected abstract void SetClose(float lerp);

        protected override void InitiateReset()
        {
            SetOpen(0);
        }

        protected override void InitiateOpen()
        {
            InputLock = true;
            BgLock = true;
            ContentEnabled = true;

            OnOpenBegin?.Invoke();

            if (gameObject.activeSelf)
            {
                if (useMEC)
                {
                    Timing.KillCoroutines(openHandle);
                    openHandle = Timing.RunCoroutine(MecOpenRoutine());
                }
                else
                {
                    StartCoroutine(OpenRoutine());
                }
            }
            else
            {
                InputLock = false;
                BgLock = false;
                ContentEnabled = true;

                SetOpen(1);
                OnOpenComplete();
            }
        }

        private IEnumerator<float> MecOpenRoutine()
        {
            float lerp = currentOpenAmount;
            float t = lerp * Duration;

            do
            {
                yield return Timing.WaitForOneFrame;

                SetOpen(lerp);

                t += Time.deltaTime;
                lerp = t / Duration;

                if (!IsOpening)
                {
                    yield break;
                }
            } while (lerp < 1);

            if (IsOpening)
            {
                InputLock = false;
                BgLock = false;
                ContentEnabled = true;

                SetOpen(1);
                OnOpenComplete();
            }
        }

        private IEnumerator OpenRoutine()
        {
            float lerp = currentOpenAmount;
            float t = lerp * Duration;

            do
            {
                yield return new WaitForEndOfFrame();

                SetOpen(lerp);

                t += Time.deltaTime;
                lerp = t / Duration;

                if (!IsOpening)
                {
                    yield break;
                }
            } while (lerp < 1);

            if (IsOpening)
            {
                InputLock = false;
                BgLock = false;
                ContentEnabled = true;

                SetOpen(1);
                OnOpenComplete();
            }
        }

        protected override void InitiateClose()
        {
            InputLock = true;
            BgLock = true;

            OnCloseBegin?.Invoke();

            if (gameObject.activeSelf)
            {
                if (useMEC)
                {
                    Timing.KillCoroutines(closeHandle);
                    closeHandle = Timing.RunCoroutine(MecCloseRoutine());
                }
                else
                {
                    StartCoroutine(CloseRoutine());
                }
            }
            else
            {
                InputLock = false;
                BgLock = false;
                ContentEnabled = false;

                SetOpen(0);
                OnCloseComplete();
            }
        }

        private IEnumerator<float> MecCloseRoutine()
        {
            float lerp = currentOpenAmount;
            float t = lerp * Duration;

            do
            {
                yield return Timing.WaitForOneFrame;
                SetOpen(lerp);

                t -= Time.deltaTime;
                lerp = t / Duration;

                if (!IsClosing)
                {
                    yield break;
                }
            } while (lerp > 0);

            if (IsClosing)
            {
                InputLock = false;
                BgLock = false;
                ContentEnabled = false;

                SetOpen(0);
                OnCloseComplete();
            }
        }

        private IEnumerator CloseRoutine()
        {
            float lerp = currentOpenAmount;
            float t = lerp * Duration;

            do
            {
                yield return new WaitForEndOfFrame();
                SetOpen(lerp);

                t -= Time.deltaTime;
                lerp = t / Duration;

                if (!IsClosing)
                {
                    yield break;
                }
            } while (lerp > 0);

            if (IsClosing)
            {
                InputLock = false;
                BgLock = false;
                ContentEnabled = false;

                SetOpen(0);
                OnCloseComplete();
            }
        }

        private void OnDestroy()
        {
            Timing.KillCoroutines(openHandle);
            Timing.KillCoroutines(closeHandle);
        }
    }

    public abstract class LerpView<TModel> : BaseView<TModel>
        where TModel : BaseModel, new()
    {
        [SerializeField, BoxGroup("Base Components")]
        private float duration = .5f;

        /// <summary>
        /// Enabling this parameter makes view use Timing Coroutines (see MEC)
        /// </summary>
        [SerializeField, BoxGroup("Base Components")]
        protected bool useMEC = false;

        protected float currentOpenAmount;

        private CoroutineHandle openHandle;
        private CoroutineHandle closeHandle;

        public float Duration
        {
            get { return duration; }
        }

        /// <summary>
        /// Returns a evaluated result for lerp value which is in [0, 1]
        /// </summary>
        /// <param name="lerp"></param>
        /// <returns></returns>
        public abstract float Evaluate(float lerp);

        /// <summary>
        /// Set Animation state evaluating above.
        /// </summary>
        /// <param name="lerp"></param>
        protected abstract void SetOpen(float lerp);

        /// <summary>
        /// Set Animation state evaluating above.
        /// </summary>
        /// <param name="lerp"></param>
        protected abstract void SetClose(float lerp);

        protected override void InitiateReset()
        {
            SetOpen(0);
        }

        protected override void InitiateOpen()
        {
            InputLock = true;
            BgLock = true;
            ContentEnabled = true;

            OnOpenBegin?.Invoke();

            if (gameObject.activeSelf)
            {
                if (useMEC)
                {
                    Timing.KillCoroutines(openHandle);
                    openHandle = Timing.RunCoroutine(MecOpenRoutine());
                }
                else
                {
                    StartCoroutine(OpenRoutine());
                }
            }
            else
            {
                InputLock = false;
                BgLock = false;
                ContentEnabled = true;

                SetOpen(1);
                OnOpenComplete();
            }
        }

        private IEnumerator<float> MecOpenRoutine()
        {
            float lerp = currentOpenAmount;
            float t = lerp * Duration;

            do
            {
                yield return Timing.WaitForOneFrame;

                SetOpen(lerp);

                t += Time.deltaTime;
                lerp = t / Duration;

                if (!IsOpening)
                {
                    yield break;
                }
            } while (lerp < 1);

            if (IsOpening)
            {
                InputLock = false;
                BgLock = false;
                ContentEnabled = true;

                SetOpen(1);
                OnOpenComplete();
            }
        }

        private IEnumerator OpenRoutine()
        {
            float lerp = currentOpenAmount;
            float t = lerp * Duration;

            do
            {
                yield return new WaitForEndOfFrame();

                SetOpen(lerp);

                t += Time.deltaTime;
                lerp = t / Duration;

                if (!IsOpening)
                {
                    yield break;
                }
            } while (lerp < 1);

            if (IsOpening)
            {
                InputLock = false;
                BgLock = false;
                ContentEnabled = true;

                SetOpen(1);
                OnOpenComplete();
            }
        }

        protected override void InitiateClose()
        {
            InputLock = true;
            BgLock = true;

            OnCloseBegin?.Invoke();

            if (gameObject.activeSelf)
            {
                if (useMEC)
                {
                    Timing.KillCoroutines(closeHandle);
                    closeHandle = Timing.RunCoroutine(MecCloseRoutine());
                }
                else
                {
                    StartCoroutine(CloseRoutine());
                }
            }
            else
            {
                InputLock = false;
                BgLock = false;
                ContentEnabled = false;

                SetOpen(0);
                OnCloseComplete();
            }
        }

        private IEnumerator<float> MecCloseRoutine()
        {
            float lerp = currentOpenAmount;
            float t = lerp * Duration;

            do
            {
                yield return Timing.WaitForOneFrame;
                SetOpen(lerp);

                t -= Time.deltaTime;
                lerp = t / Duration;

                if (!IsClosing)
                {
                    yield break;
                }
            } while (lerp > 0);

            if (IsClosing)
            {
                InputLock = false;
                BgLock = false;
                ContentEnabled = false;

                SetOpen(0);
                OnCloseComplete();
            }
        }

        private IEnumerator CloseRoutine()
        {
            float lerp = currentOpenAmount;
            float t = lerp * Duration;

            do
            {
                yield return new WaitForEndOfFrame();
                SetOpen(lerp);

                t -= Time.deltaTime;
                lerp = t / Duration;

                if (!IsClosing)
                {
                    yield break;
                }
            } while (lerp > 0);

            if (IsClosing)
            {
                InputLock = false;
                BgLock = false;
                ContentEnabled = false;

                SetOpen(0);
                OnCloseComplete();
            }
        }

        private void OnDestroy()
        {
            Timing.KillCoroutines(openHandle);
            Timing.KillCoroutines(closeHandle);
        }
    }
}