﻿namespace MatchinghamGames.StockholmCore.Runtime
{
    public abstract class EmptyView : BaseView
    {
        protected override void InitiateReset()
        {
            ContentRoot.SetActive(false);
        }

        protected override void InitiateOpen()
        {
            OnOpenBegin?.Invoke();
            ContentRoot.SetActive(true);
            OnOpenComplete();
        }

        protected override void InitiateClose()
        {
            OnCloseBegin?.Invoke();
            ContentRoot.SetActive(false);
            OnCloseComplete();
        }
    }
    
    public abstract class EmptyView<TModel> : BaseView<TModel>
    where TModel : BaseModel, new()
    {
        protected override void InitiateReset()
        {
            ContentRoot.SetActive(false);
        }

        protected override void InitiateOpen()
        {
            OnOpenBegin?.Invoke();
            ContentRoot.SetActive(true);
            OnOpenComplete();
        }

        protected override void InitiateClose()
        {
            OnCloseBegin?.Invoke();
            ContentRoot.SetActive(false);
            OnCloseComplete();
        }
    }
}
