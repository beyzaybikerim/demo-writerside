﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

#if TMP || UGUI_2_OR_NEWER
using TMPro;
#endif

namespace MatchinghamGames.StockholmCore.Runtime
{
    public abstract class LerpFadeView : LerpView
    {
        [SerializeField, BoxGroup("Color Components")]
        private SpriteRenderer[] sprtRenderers;

        [SerializeField, BoxGroup("Color Components")]
        private Image[] images;

#if TMP || UGUI_2_OR_NEWER
        [SerializeField, BoxGroup("Color Components")]
        private TMP_Text[] texts;
#endif
        [SerializeField, BoxGroup("Color Components")]
        private Text[] legacyTexts;

        protected override void OnValidate()
        {
            // base.OnValidate(); // Content root validation is not necessary in this view type
            if (ContentRoot == null)
            {
                ContentRoot = gameObject;
            }

            bool isElementSet = false;

            sprtRenderers = ContentRoot.GetComponents<SpriteRenderer>();
            images = ContentRoot.GetComponents<Image>();
            legacyTexts = ContentRoot.GetComponents<Text>();

            isElementSet = (sprtRenderers != null && sprtRenderers.Length > 0) ||
                           (images != null && images.Length > 0) ||
                           (legacyTexts != null && legacyTexts.Length > 0);

#if TMP || UGUI_2_OR_NEWER
            texts = ContentRoot.GetComponents<TMP_Text>();
            isElementSet = isElementSet || (texts != null && texts.Length > 0);
#endif

            if (!isElementSet)
            {
                throw new NotSupportedException("There is no suitable component with color element.");
            }
        }

        protected override void SetOpen(float lerp)
        {
            Color currentColor;
            currentOpenAmount = lerp;

            foreach (var sprtRenderer in sprtRenderers)
            {
                currentColor = sprtRenderer.color;
                sprtRenderer.color = new Color(currentColor.r, currentColor.g, currentColor.b, Evaluate(lerp));
            }

            foreach (var image in images)
            {
                currentColor = image.color;
                image.color = new Color(currentColor.r, currentColor.g, currentColor.b, Evaluate(lerp));
            }

#if TMP || UGUI_2_OR_NEWER
            foreach (var text in texts)
            {
                currentColor = text.color;
                text.color = new Color(currentColor.r, currentColor.g, currentColor.b, Evaluate(lerp));
            }
#endif

            foreach (var text in legacyTexts)
            {
                currentColor = text.color;
                text.color = new Color(currentColor.r, currentColor.g, currentColor.b, Evaluate(lerp));
            }
        }

        protected override void SetClose(float lerp)
        {
            SetOpen(1 - lerp);
        }
    }

    public abstract class LerpFadeView<TModel> : LerpView<TModel>
        where TModel : BaseModel, new()
    {
        [SerializeField, BoxGroup("Color Components")]
        private SpriteRenderer[] sprtRenderers;

        [SerializeField, BoxGroup("Color Components")]
        private Image[] images;

#if TMP || UGUI_2_OR_NEWER
        [SerializeField, BoxGroup("Color Components")]
        private TMP_Text[] texts;
#endif
        [SerializeField, BoxGroup("Color Components")]
        private Text[] legacyTexts;

        protected override void OnValidate()
        {
            // base.OnValidate(); // Content root validation is not necessary in this view type
            if (ContentRoot == null)
            {
                ContentRoot = gameObject;
            }

            bool isElementSet = false;

            sprtRenderers = ContentRoot.GetComponents<SpriteRenderer>();
            images = ContentRoot.GetComponents<Image>();
            legacyTexts = ContentRoot.GetComponents<Text>();

            isElementSet = (sprtRenderers != null && sprtRenderers.Length > 0) ||
                           (images != null && images.Length > 0) ||
                           (legacyTexts != null && legacyTexts.Length > 0);

#if TMP || UGUI_2_OR_NEWER
            texts = ContentRoot.GetComponents<TMP_Text>();
            isElementSet = isElementSet || (texts != null && texts.Length > 0);
#endif

            if (!isElementSet)
            {
                throw new NotSupportedException("There is no suitable component with color element.");
            }
        }

        protected override void SetOpen(float lerp)
        {
            Color currentColor;
            currentOpenAmount = lerp;

            foreach (var sprtRenderer in sprtRenderers)
            {
                currentColor = sprtRenderer.color;
                sprtRenderer.color = new Color(currentColor.r, currentColor.g, currentColor.b, Evaluate(lerp));
            }

            foreach (var image in images)
            {
                currentColor = image.color;
                image.color = new Color(currentColor.r, currentColor.g, currentColor.b, Evaluate(lerp));
            }

#if TMP || UGUI_2_OR_NEWER
            foreach (var text in texts)
            {
                currentColor = text.color;
                text.color = new Color(currentColor.r, currentColor.g, currentColor.b, Evaluate(lerp));
            }
#endif

            foreach (var text in legacyTexts)
            {
                currentColor = text.color;
                text.color = new Color(currentColor.r, currentColor.g, currentColor.b, Evaluate(lerp));
            }
        }

        protected override void SetClose(float lerp)
        {
            SetOpen(1 - lerp);
        }
    }
}