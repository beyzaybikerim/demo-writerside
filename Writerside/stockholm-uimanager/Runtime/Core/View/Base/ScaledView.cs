namespace MatchinghamGames.StockholmCore.Runtime
{
    public abstract class ScaledView : LerpScaleView
    {
        public override float Evaluate(float lerp)
        {
            return lerp;
        }
    }
    
    public abstract class ScaledView<TModel> : LerpScaleView<TModel>
        where TModel : BaseModel, new()
    {
        public override float Evaluate(float lerp)
        {
            return lerp;
        }
    }
}
