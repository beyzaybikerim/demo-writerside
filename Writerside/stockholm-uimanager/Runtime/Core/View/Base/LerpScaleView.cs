﻿using UnityEngine;

namespace MatchinghamGames.StockholmCore.Runtime
{
    public abstract class LerpScaleView : LerpView
    {
        protected override void SetOpen(float lerp)
        {
            currentOpenAmount = lerp;
            if (ContentRoot == null)
            {
                return;
            }

            ContentRoot.transform.localScale = Evaluate(lerp) * Vector2.one;
        }

        protected override void SetClose(float lerp)
        {
            SetOpen(1 - lerp);
        }
    }

    public abstract class LerpScaleView<TModel> : LerpView<TModel>
        where TModel : BaseModel, new()
    {
        protected override void SetOpen(float lerp)
        {
            if (ContentRoot == null)
            {
                return;
            }

            ContentRoot.transform.localScale = Evaluate(lerp) * Vector2.one;
        }

        protected override void SetClose(float lerp)
        {
            SetOpen(1 - lerp);
        }
    }
}