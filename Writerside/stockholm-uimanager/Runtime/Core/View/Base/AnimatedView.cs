using Sirenix.OdinInspector;
using UnityEngine;

namespace MatchinghamGames.StockholmCore.Runtime
{
    public abstract class AnimatedView : BaseView, IAnimatedView
    {
#pragma warning disable 649
        [SerializeField, BoxGroup("Base Components")]
        string openTrigger = "Open";

        [SerializeField, BoxGroup("Base Components")]
        string closeTrigger = "Close";

        [SerializeField, BoxGroup("Base Components")]
        string idleStateName = "Idle";

        [SerializeField, BoxGroup("Base Components")]
        Animator animatorComp;
#pragma warning restore 649

        public Animator CachedAnimator
        {
            get
            {
                if (animatorComp == null)
                {
                    animatorComp = GetComponent<Animator>();
                }

                return animatorComp;
            }
        }

        protected virtual void Reset()
        {
            resetBeforeOpenCall = true;
            InputLock = false;
            BgLock = false;
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (!animatorComp)
            {
                ContentRoot.AddComponent<Animator>();
                animatorComp = ContentRoot.GetComponent<Animator>();
            }

            if (animatorComp.runtimeAnimatorController == null)
            {
                var sampleAnimController = Resources.Load<RuntimeAnimatorController>("SampleAnimController");
                animatorComp.runtimeAnimatorController = sampleAnimController;
            }
        }

        protected override void InitiateReset()
        {
            ResetAnimation(idleStateName);
        }

        private void ResetAnimation(string entryState)
        {
            if (CachedAnimator == null || CachedAnimator.gameObject == null)
            {
                return;
            }

            CachedAnimator.ResetTrigger(openTrigger);
            CachedAnimator.ResetTrigger(closeTrigger);

            CachedAnimator.Play(entryState, -1, 0f);
        }

        public void Open(bool forceOpen)
        {
            if (forceOpen)
            {
                ResetView();
            }

            Open();
        }

        protected override void InitiateOpen()
        {
            ContentEnabled = true;

            SetTrigger(true);
        }

        protected override void InitiateClose()
        {
            SetTrigger(false);
        }

        private void SetTrigger(bool isOpen)
        {
            if (CachedAnimator == null || CachedAnimator.gameObject == null)
            {
                return;
            }

            if (isOpen)
            {
                CachedAnimator.SetTrigger(openTrigger);
                CachedAnimator.ResetTrigger(closeTrigger);
            }
            else
            {
                CachedAnimator.SetTrigger(closeTrigger);
                CachedAnimator.ResetTrigger(openTrigger);
            }
        }

        public void OnOpenAnimationStart()
        {
            OnOpenBegin?.Invoke();
        }

        public void OnOpenAnimationEnd()
        {
            OnOpenComplete();
        }

        public void OnCloseAnimationStart()
        {
            OnCloseBegin?.Invoke();
        }

        public void OnCloseAnimationEnd()
        {
            OnCloseComplete();
        }
    }

    public abstract class AnimatedView<TModel> : BaseView<TModel>, IAnimatedView
        where TModel : BaseModel, new()
    {
#pragma warning disable 649
        [SerializeField, BoxGroup("Base Components")]
        string openTrigger = "Open";

        [SerializeField, BoxGroup("Base Components")]
        string closeTrigger = "Close";

        [SerializeField, BoxGroup("Base Components")]
        string idleStateName = "Idle";

        [SerializeField, BoxGroup("Base Components")]
        Animator animatorComp;
#pragma warning restore 649

        public Animator CachedAnimator
        {
            get
            {
                if (animatorComp == null)
                {
                    animatorComp = GetComponent<Animator>();
                }

                return animatorComp;
            }
        }

        protected virtual void Reset()
        {
            resetBeforeOpenCall = true;
            InputLock = false;
            BgLock = false;
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            if (!animatorComp)
            {
                ContentRoot.AddComponent<Animator>();
                animatorComp = ContentRoot.GetComponent<Animator>();
            }

            if (animatorComp.runtimeAnimatorController == null)
            {
                var sampleAnimController = Resources.Load<RuntimeAnimatorController>("SampleAnimController");
                animatorComp.runtimeAnimatorController = sampleAnimController;
            }
        }

        protected override void InitiateReset()
        {
            ResetAnimation(idleStateName);
        }

        private void ResetAnimation(string entryState)
        {
            if (CachedAnimator == null || CachedAnimator.gameObject == null)
            {
                return;
            }

            CachedAnimator.ResetTrigger(openTrigger);
            CachedAnimator.ResetTrigger(closeTrigger);

            CachedAnimator.Play(entryState, -1, 0f);
        }

        public void Open(bool forceOpen)
        {
            if (forceOpen)
            {
                ResetView();
            }

            Open();
        }

        protected override void InitiateOpen()
        {
            ContentEnabled = true;

            SetTrigger(true);
        }

        protected override void InitiateClose()
        {
            SetTrigger(false);
        }

        private void SetTrigger(bool isOpen)
        {
            if (CachedAnimator == null || CachedAnimator.gameObject == null)
            {
                return;
            }

            if (isOpen)
            {
                CachedAnimator.SetTrigger(openTrigger);
                CachedAnimator.ResetTrigger(closeTrigger);
            }
            else
            {
                CachedAnimator.SetTrigger(closeTrigger);
                CachedAnimator.ResetTrigger(openTrigger);
            }
        }

        public void OnOpenAnimationStart()
        {
            OnOpenBegin?.Invoke();
        }

        public void OnOpenAnimationEnd()
        {
            OnOpenComplete();
        }

        public void OnCloseAnimationStart()
        {
            OnCloseBegin?.Invoke();
        }

        public void OnCloseAnimationEnd()
        {
            OnCloseComplete();
        }
    }
}