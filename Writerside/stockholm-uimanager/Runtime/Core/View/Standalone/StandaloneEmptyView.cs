﻿namespace MatchinghamGames.StockholmCore.Runtime
{
    public sealed class StandaloneEmptyView : EmptyView
    {
        protected override void AddHandlers()
        {
        }

        protected override void RemoveHandlers()
        {
        }
    }
}