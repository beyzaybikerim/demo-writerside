﻿namespace MatchinghamGames.StockholmCore.Runtime
{
    public interface IAnimatedView
    {
        public void OnOpenAnimationStart();
        public void OnOpenAnimationEnd();
        public void OnCloseAnimationStart();
        public void OnCloseAnimationEnd();
    }
}
