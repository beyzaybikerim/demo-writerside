using MatchinghamGames.StockholmCore.Runtime;

namespace MatchinghamGames.StockholmCore.Demo.UI.Models
{
    public class DemoWinModel : BaseModel
    {
        // Define your properties here
        public const string Gold = nameof(Gold);
        public const string LevelNo = nameof(LevelNo);
        
        protected override void Initialize()
        {
            // Initialize your parameters here
            Set(Gold, 100);
            Set(LevelNo, 0);
        }
    }
}
