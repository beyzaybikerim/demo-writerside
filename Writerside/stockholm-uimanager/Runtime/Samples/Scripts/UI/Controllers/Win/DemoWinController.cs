using MatchinghamGames.StockholmCore.Demo.UI.Models;
using MatchinghamGames.StockholmCore.Demo.UI.Views;
using MatchinghamGames.StockholmCore.Runtime;
using UnityEngine;

namespace MatchinghamGames.StockholmCore.Demo.UI.Controllers
{
    public class DemoWinController : Controller<DemoWinView, DemoWinModel>
    {
        private static int LevelNo = 23;

        protected override void Initialize()
        {
            // Define what you would want to see in Awake here
            base.Initialize();
            Model.Set(DemoWinModel.Gold, (int)Model.Get(DemoWinModel.Gold) + Random.Range(0, 1000));
            Model.Set(DemoWinModel.LevelNo, ++LevelNo);
        }

        protected override void AddHandlers()
        {
            // Define the functionality of buttons etc. here..
            View.OnCloseClick += OnCloseClickAction;
            View.OnCloseBegin += () =>
            {
                foreach (var v in View.subviews)
                {
                    v.Close();
                }
            };
            View.OnOpenBegin += () =>
            {
                foreach (var v in View.subviews)
                {
                    v.ResetView();
                }
            };
            View.OnOpenEnd += () =>
            {
                foreach (var v in View.subviews)
                {
                    v.Open();
                }
            };
            Model.Subscribe(DemoWinModel.Gold, OnGoldUpdate);
            Model.Subscribe(DemoWinModel.LevelNo, OnLevelNoUpdate);
        }

        // Define your methods here
        private void OnCloseClickAction()
        {
            Close();
        }

        private void OnGoldUpdate(bool init, object value)
        {
            Debug.Log("Gold Updated to " + value);
            View.CurrentGoldText.text = value.ToString();
        }

        private void OnLevelNoUpdate(bool init, object value)
        {
            Debug.Log("Level Updated to " + value);
            View.TitleText.text = $"Level {value}";
        }
    }
}