using MatchinghamGames.StockholmCore.Demo.UI.Models;
using MatchinghamGames.StockholmCore.Runtime;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace MatchinghamGames.StockholmCore.Demo.UI.Views
{ 
	public class DemoWinView : AnimatedView<DemoWinModel>
    {
		/* There are some sample code which you should delete later */
        [SerializeField] private Button _closeButton;
		[SerializeField] public Text CurrentGoldText;
		[SerializeField] public Text TitleText;
		[SerializeField] public View[] subviews;
		
		public event UnityAction OnCloseClick;
		private void OnCloseClickListener() => OnCloseClick?.Invoke();
		
        protected override void AddHandlers()
        {
            // Define your interactable component functions here like button click
            _closeButton.onClick.AddListener(OnCloseClickListener);
        }
        
        protected override void RemoveHandlers()
        {
            // Remove your interactable component functions here like button click
            _closeButton.onClick.RemoveListener(OnCloseClickListener);
        }
    }
}

