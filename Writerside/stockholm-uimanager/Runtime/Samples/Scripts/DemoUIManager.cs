using MatchinghamGames.StockholmCore.Demo.UI.Controllers;
using MatchinghamGames.StockholmCore.Runtime;
using UnityEngine;
using UnityEngine.UI;

namespace MatchinghamGames.StockholmCore.Demo
{
    public class DemoUIManager : Stockholm
    {
        [SerializeField] private Button _mountNewButton;
        [SerializeField] private Button _unmountNewButton;
        [SerializeField] private Button _openNewButton;
        [SerializeField] private Button _closeNewButton;
        
        [SerializeField] private Button _unmountCurrentButton;
        [SerializeField] private Button _openCurrentButton;
        [SerializeField] private Button _closeCurrentButton;

        [SerializeField] private GameObject _currentCtrlOperationsContainer;
        
        [SerializeField] private DemoWinController _currentWin;
        private DemoWinController _newWin;
        
        protected override void Awake()
        {
            base.Awake();
            Register("OldWin", _currentWin);
            
            _unmountCurrentButton.onClick.AddListener(() =>
            {
                Unmount("OldWin");
                _currentCtrlOperationsContainer.SetActive(false);
            });
            _openCurrentButton.onClick.AddListener(() =>
            {
                if (_currentWin)
                {
                    _currentWin.Open();
                }
            });
            _closeCurrentButton.onClick.AddListener(() =>
            {
                if (_currentWin)
                {
                    _currentWin.Close();
                }
            });
            
            _mountNewButton.onClick.AddListener(() =>
            {
                _newWin = MountSingle<DemoWinController>();
                _newWin.gameObject.SetActive(true);
                // _newWin = Mount<DemoWinController>(); // this would mount another on top of it.
            });
            _unmountNewButton.onClick.AddListener(() =>
            {
                if (_newWin)
                {
                    Unmount(_newWin);    
                }
            });
            _openNewButton.onClick.AddListener(() =>
            {
                if (_newWin)
                {
                    _newWin.Open();
                }
            });
            _closeNewButton.onClick.AddListener(() =>
            {
                if (_newWin)
                {
                    _newWin.Close();
                }
            });
        }
    }
}
