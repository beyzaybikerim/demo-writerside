using System;
using UnityEngine;

namespace MatchinghamGames.StockholmCore.Runtime.Helpers
{
    public class ResourcesUiLoader : IUiLoader
    {
        public const string DEFAULT_UI_FOLDER = "Stockholm";
        public const string DEFAULT_RESOURCES_PATH = "Assets/MatchinghamGames/Resources/" + DEFAULT_UI_FOLDER;

        public TController Load<TController>()
            where TController : Controller
        {
            return Resources.Load<TController>(
                $"{DEFAULT_UI_FOLDER}/{typeof(TController).Name}"); // Assumption, the ui prefab name is the same as the controller type name
        }

        public TController Load<TController>(string name)
            where TController : Controller
        {
            return Resources.Load<TController>($"{DEFAULT_UI_FOLDER}/{name}");
        }

        public void LoadAsync<TController>(Action<TController> onLoad) where TController : Controller
        {
            LoadAsync<TController>($"{DEFAULT_UI_FOLDER}/{typeof(TController).Name}", onLoad);
        }

        public void LoadAsync<TController>(string name, Action<TController> onLoad) where TController : Controller
        {
            var request = Resources.LoadAsync<TController>($"{DEFAULT_UI_FOLDER}/{name}");

            request.completed += op =>
            {
                if (request.asset != null)
                {
                    onLoad?.Invoke(request.asset as TController);
                }
                else
                {
                    onLoad?.Invoke(null);
                }
            };
        }
    }
}