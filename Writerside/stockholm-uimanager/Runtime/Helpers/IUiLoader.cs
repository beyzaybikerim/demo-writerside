using System;
using UnityEngine;

namespace MatchinghamGames.StockholmCore.Runtime.Helpers
{
    public interface IUiLoader
    {
        TController Load<TController>() where TController : Controller;

        TController Load<TController>(string name) where TController : Controller;

        void LoadAsync<TController>(Action<TController> onLoad) where TController : Controller;

        void LoadAsync<TController>(string name, Action<TController> onLoad) where TController : Controller;
    }
}