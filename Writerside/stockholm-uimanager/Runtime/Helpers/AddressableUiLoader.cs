﻿using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace MatchinghamGames.StockholmCore.Runtime.Helpers
{
    public class AddressableUiLoader : IUiLoader
    {
        public TController Load<TController>()
            where TController : Controller
        {
            throw new NotSupportedException($"Loading in line is not supported in Addressable systems");
        }

        public TController Load<TController>(string name)
            where TController : Controller
        {
            throw new NotSupportedException($"Loading in line is not supported in Addressable systems");
        }

        public void LoadAsync<TController>(Action<TController> onLoad) where TController : Controller
        {
            LoadAsync<TController>(typeof(TController).Name, onLoad);
        }

        public void LoadAsync<TController>(string name, Action<TController> onLoad) where TController : Controller
        {
            var handle = Addressables.LoadAssetAsync<TController>(name);
            
            handle.Completed += op =>
            {
                if (op.Result != null)
                {
                    onLoad?.Invoke(op.Result);
                }
                else
                {
                    onLoad?.Invoke(null);
                }
            };
        }
    }
}