using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MatchinghamGames.StockholmCore.Runtime.Helpers
{
    public interface IObjectManager
    {
        T Spawn<T>(T original, Transform uiRoot) where T : Object;
        void Dispose<T>(T instance) where T : Object;
    }
}