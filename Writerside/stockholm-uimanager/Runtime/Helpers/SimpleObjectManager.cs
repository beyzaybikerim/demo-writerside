using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MatchinghamGames.StockholmCore.Runtime.Helpers
{
    public class SimpleObjectManager : IObjectManager
    {
        public T Spawn<T>(T original, Transform uiRoot) where T : Object
        {
            return Object.Instantiate(original, uiRoot);
        }

        public void Dispose<T>(T instance) where T : Object
        {
            if (instance == null)
            {
                Debug.LogError("Object reference is not set to an instance of an object: instance is null");
                return;
            }

            switch (instance)
            {
                case GameObject go:
                    Object.Destroy(go);
                    break;

                case Component comp:
                    if (comp.gameObject != null)
                    {
                        Object.Destroy(comp.gameObject);
                    }
                    else
                    {
                        Debug.LogError("Object reference is not set to an instance of an object: Component gameobject is null");
                    }

                    break;

                default:
                    Object.Destroy(instance);
                    break;
            }
        }
    }
}