using MatchinghamGames.Handyman;
using MatchinghamGames.StockholmCore.Runtime.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = System.Object;

namespace MatchinghamGames.StockholmCore.Runtime
{
    public class Stockholm : SingletonBehaviour<Stockholm>
    {
        public const string UIRootTag = "UIRoot";

#pragma warning disable 649
        [SerializeField] protected Transform defaultRoot;
#pragma warning restore 649

        protected IObjectManager manager;
        protected IUiLoader loader;

        private Dictionary<string, List<Controller>> activeControllers; // Controllers grouped by their prefab name

        /// <summary>
        /// The parent of the transform of a controller when no parent is specified.
        /// </summary>
        protected Transform UiRoot
        {
            get
            {
                if (defaultRoot == null)
                {
                    FetchRoot();
                }

                return defaultRoot;
            }
        }

        /// <summary>
        /// The Load Functionality to manage fetching from directory at mount.
        /// </summary>
        public IUiLoader Loader
        {
            get => loader ??= new ResourcesUiLoader();
            set => loader = value;
        }

        /// <summary>
        /// The Spawn Functionality to manage instantiating gameobjects.
        /// </summary>
        public IObjectManager Manager
        {
            get => manager ??= new SimpleObjectManager();
            set => manager = value;
        }

        protected virtual void Awake()
        {
            activeControllers = new Dictionary<string, List<Controller>>();
        }

        private void FetchRoot()
        {
            defaultRoot = GameObject.FindGameObjectWithTag(UIRootTag)?.transform;

            if (defaultRoot == null)
            {
                throw new MissingReferenceException($"Default root is not assigned in {nameof(Stockholm)} and " +
                                                    $"cannot find an object with \"{UIRootTag}\" tag.");
            }
        }

        /// <summary>
        /// Register already created UI instance.
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="name">Prefab name</param>
        protected void Register<TController>(string name, TController controller) where TController : Controller
        {
            if (activeControllers.TryGetValue(name, out var cache))
            {
                cache.Add(controller);
            }
            else
            {
                activeControllers.Add(name, new List<Controller>()
                {
                    controller
                });
            }
        }

        /// <summary>
        /// Loads the selected Controller and instantiates it on default canvas given on,
        /// if already mounted one, returns it.
        /// If previously mounted several, returns first found.
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <returns></returns>
        public TController MountSingle<TController>() where TController : Controller
        {
            return MountSingle<TController>(UiRoot);
        }

        /// <summary>
        /// Loads the selected Controller and instantiates it on default canvas given on,
        /// if already mounted one, returns it.
        /// If previously mounted several, returns first found.
        /// </summary>
        /// <param name="name">Name of the prefab</param>
        /// <typeparam name="TController"></typeparam>
        /// <returns></returns>
        public TController MountSingle<TController>(string name) where TController : Controller
        {
            return MountSingle<TController>(name, UiRoot);
        }

        /// <summary>
        /// Loads the selected Controller and instantiates it on default canvas given on,
        /// if already mounted one, returns it.
        /// If previously mounted several, returns first found.
        /// </summary>
        /// <param name="root">Name of the prefab</param>
        /// <typeparam name="TController"></typeparam>
        /// <returns></returns>
        public TController MountSingle<TController>(Transform root) where TController : Controller
        {
            string name = typeof(TController).Name;
            return MountSingle<TController>(name, root);
        }

        /// <summary>
        /// Loads the selected Controller and instantiates it on given transform,
        /// if already mounted one, returns it.
        /// If previously mounted several, returns first found.
        /// </summary>
        /// <param name="root">Parent Transform object</param>
        /// <param name="name">Name of the prefab</param>
        /// <typeparam name="TController"></typeparam>
        /// <returns></returns>
        public TController MountSingle<TController>(string name, Transform root) where TController : Controller
        {
#if UNITY_EDITOR
            var allFound = FindAll<TController>();

            if (allFound.Count > 1)
            {
                Debug.LogError($"{nameof(MountSingle)} detected more than one instance of the object, it already exists more than one at this point, returning first found. This is an editor time log.");
            }
#endif
            var ctrlInstance = FindFirst<TController>(name);

            if (ctrlInstance != null)
            {
                ctrlInstance.transform.SetParent(root);
                return ctrlInstance;
            }
            else
            {
                return Mount<TController>(name, root);
            }
        }

        /// <summary>
        /// Loads the selected Controller and instantiates it on default canvas given on.
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <param name="name">Name of the prefab</param>
        /// <returns></returns>
        public TController Mount<TController>() where TController : Controller
        {
            return Mount<TController>(UiRoot);
        }

        /// <summary>
        /// Loads the selected Controller and instantiates it on default canvas given on.
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <param name="name">Name of the prefab</param>
        /// <returns></returns>
        public TController Mount<TController>(string name) where TController : Controller
        {
            return Mount<TController>(name, UiRoot);
        }

        /// <summary>
        /// Loads the selected Controller and instantiates it on given transform, logically a Canvas.
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <param name="root"></param>
        /// <returns></returns>
        public TController Mount<TController>(Transform root) where TController : Controller
        {
            string name = typeof(TController).Name;

            return Mount<TController>(name, root);
        }

        /// <summary>
        /// Loads the selected Controller and instantiates it on given transform, logically a Canvas.
        /// </summary>
        /// <param name="name">Name of the prefab</param>
        /// <param name="root">Parent Transform object</param>
        /// <typeparam name="TController">Controller type</typeparam>
        /// <returns></returns>
        public TController Mount<TController>(string name, Transform root) where TController : Controller
        {
            var controller = Loader.Load<TController>(name);

            if (controller == null)
            {
                Debug.LogError(name + " cannot not be loaded.");
                return null;
            }

            var instance = Manager.Spawn(controller, root);

            if (instance == null)
            {
                Debug.LogError(typeof(TController).Name + " cannot not be spawned.");
                return null;
            }

            instance.gameObject.SetActive(false);

            TController instanceController = instance.GetComponent<TController>();

            Register(name, instanceController);

            return instance;
        }

        /// <summary>
        /// [Experimental]
        /// Loads the selected Controller and instantiates it on given transform, logically a Canvas.
        /// </summary>
        /// <param name="onLoad"></param>
        /// <typeparam name="TController"></typeparam>
        public void MountSingleAsync<TController>(Action<TController> onLoad) where TController : Controller
        {
            MountSingleAsync<TController>(UiRoot, onLoad);
        }

        /// <summary>
        /// [Experimental]
        /// Loads the selected Controller and instantiates it on given transform, logically a Canvas.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="onLoad"></param>
        /// <typeparam name="TController"></typeparam>
        public void MountSingleAsync<TController>(Transform root, Action<TController> onLoad)
            where TController : Controller
        {
#if UNITY_EDITOR
            var allFound = FindAll<TController>();

            if (allFound.Count > 1)
            {
                Debug.LogError($"{nameof(MountSingle)} detected more than one instance of the object, it already exists more than one at this point, returning first found. This is an editor time log.");
            }
#endif

            var ctrlInstance = FindFirst<TController>();

            if (ctrlInstance != null)
            {
                ctrlInstance.transform.SetParent(root);
                onLoad?.Invoke(ctrlInstance);
            }
            else
            {
                MountAsync<TController>(root, onLoad);
            }
        }

        /// <summary>
        /// [Experimental]
        /// Loads the selected Controller and instantiates it on given transform, logically a Canvas.
        /// </summary>
        /// <param name="onLoad"></param>
        /// <typeparam name="TController"></typeparam>
        public void MountAsync<TController>(Action<TController> onLoad) where TController : Controller
        {
            MountAsync<TController>(UiRoot, onLoad);
        }

        /// <summary>
        /// [Experimental]
        /// Loads the selected Controller and instantiates it on given transform, logically a Canvas.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="onLoad"></param>
        /// <typeparam name="TController"></typeparam>
        public void MountAsync<TController>(Transform root, Action<TController> onLoad) where TController : Controller
        {
            string name = typeof(TController).Name;
            MountAsync(name, root, onLoad);
        }

        /// <summary>
        /// [Experimental]
        /// Loads the selected Controller and instantiates it on given transform, logically a Canvas.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="root"></param>
        /// <param name="onLoad"></param>
        /// <typeparam name="TController"></typeparam>
        public void MountAsync<TController>(string name, Transform root, Action<TController> onLoad) where TController : Controller
        {
            Loader.LoadAsync<TController>(name, OnAsyncLoad);

            void OnAsyncLoad(TController controller)
            {
                if (controller == null)
                {
                    Debug.LogError($"{name} cannot not be loaded.");
                    return;
                }

                var instance = Manager.Spawn(controller, root);

                if (instance == null)
                {
                    Debug.LogError($"{name} cannot not be spawned.");
                    return;
                }

                instance.gameObject.SetActive(false);

                TController instanceController = instance.GetComponent<TController>();

                Register(name, instanceController);

                onLoad?.Invoke(instanceController);
            }
        }

        /// <summary>
        /// Removes the preloaded controller instance, destroying it.
        /// </summary>
        /// <param name="instance"></param>
        public void Unmount(Controller instance)
        {
            List<string> obsoleteKeys = new List<string>();

            foreach (var data in Instance.activeControllers)
            {
                if (data.Value.Contains(instance))
                {
                    data.Value.Remove(instance);

                    if (data.Value.Count == 0)
                    {
                        obsoleteKeys.Add(data.Key);
                    }

                    break;
                }
            }

            foreach (var key in obsoleteKeys)
            {
                if (Instance.activeControllers.ContainsKey(key))
                {
                    Instance.activeControllers.Remove(key);
                }
            }

            Manager.Dispose(instance);
        }

        /// <summary>
        /// Removes and destroys all controller instances of a given type
        /// </summary>
        /// <returns></returns>
        public void Unmount<TController>() where TController : Controller
        {
            var allControllers = FindAll<TController>();

            for (int i = allControllers.Count - 1; i >= 0; i--)
            {
                if (allControllers[i] != null)
                {
                    Unmount(allControllers[i]);    
                }                
            }
        }

        /// <summary>
        /// Removes and destroys all controller instances of a given name
        /// </summary>
        /// <returns></returns>
        public void Unmount(string name)
        {
            if (activeControllers.TryGetValue(name, out var foundControllers))
            {
                List<Controller> toUnmount = new List<Controller>();

                toUnmount.AddRange(foundControllers);

                foreach (var c in toUnmount)
                {
                    Unmount(c);
                }
            }
        }

        /// <summary>
        /// Checks if there is any controller 
        /// </summary>
        /// <returns></returns>
        public bool IsAny()
        {
            return activeControllers.Keys.Count > 0;
        }

        /// <summary>
        /// Checks if there is any controller with given type.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool IsAny<TController>() where TController : Controller
        {
            string key = typeof(TController).Name;

            return IsAny(key);
        }

        /// <summary>
        /// Checks if there is any controller with given key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool IsAny(string key)
        {
            return activeControllers.ContainsKey(key);
        }

        /// <summary>
        /// Returns the first controller instance
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <returns></returns>
        public TController FindFirst<TController>() where TController : Controller
        {
            return FindFirst<TController>(typeof(TController).Name);
        }

        /// <summary>
        /// Returns the first controller instance with a given name
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <param name="name">Name of the prefab</param>
        /// <returns></returns>
        public TController FindFirst<TController>(string name) where TController : Controller
        {
            bool nullifiedItemExists = false;

            if (activeControllers.TryGetValue(name, out var mountedControllers))
            {
                foreach (var c in mountedControllers)
                {
                    if (c == null || !c.gameObject)
                    {
                        nullifiedItemExists = true;
                        continue;
                    }

                    if (c is TController controller)
                    {
                        return controller;
                    }
                }
            }

            if (nullifiedItemExists)
            {
                CleanNullified();
            }

            return null;
        }

        /// <summary>
        /// Returns all controller instances of a specified type  
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <returns></returns>
        public List<TController> FindAll<TController>() where TController : Controller
        {
            List<TController> controllers = new List<TController>();
            bool nullifiedItemExists = false;

            foreach (var mountedControllers in activeControllers.Values)
            {
                foreach (var c in mountedControllers)
                {
                    if (c == null || !c.gameObject)
                    {
                        nullifiedItemExists = true;
                        continue;
                    }

                    if (c is TController controller)
                    {
                        controllers.Add(controller);
                    }
                }
            }

            if (nullifiedItemExists)
            {
                CleanNullified();
            }

            return controllers;
        }

        /// <summary>
        /// Cleans obsolete controlers, i.e. itself or gameobject no longer exists
        /// </summary>
        private void CleanNullified()
        {
            List<string> keys = new List<string>();
            keys.AddRange(activeControllers.Keys);

            foreach (var key in keys)
            {
                CleanNullified(key);
            }
        }

        /// <summary>
        /// Cleans obsolete controlers, i.e. itself or gameobject no longer exists
        /// </summary>
        private void CleanNullified(string key)
        {
            List<Controller> controllers = activeControllers[key];
            int count = controllers.Count;

            for (int i = count - 1; i >= 0; i--)
            {
                var c = controllers[i];

                if (c == null || !c.gameObject)
                {
                    controllers.RemoveAt(i);
                }
            }

            if (controllers.Count == 0)
            {
                activeControllers.Remove(key);
            }
        }
    }
}