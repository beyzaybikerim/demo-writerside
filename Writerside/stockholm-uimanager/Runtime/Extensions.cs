﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace MatchinghamGames.StockholmCore.Runtime
{
    public static class Extensions
    {
        /// <summary>
        /// Removes the preloaded controller instance, destroying it.
        /// Basically calls Stockholm.Instance.Unmount(Controller this);
        /// </summary>
        /// <param name="c"></param>
        public static void Unmount(this Controller c)
        {
            Stockholm.Instance.Unmount(c);
        }

        /// <summary>
        /// Loads the selected Controller and instantiates it on given transform, logically a Canvas.
        /// Basically calls Stockholm.Instance.Mount<TController>(Transform this)
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static TController Mount<TController>(this Transform t) where TController : Controller
        {
            return Stockholm.Instance.Mount<TController>(t);
        }

        /// <summary>
        /// The image fills the container recttransform with 0 margins,
        /// the color is set to be 1
        /// so that it is invisible to the eye and blocks raycast 
        /// </summary>
        /// <param name="image"></param>
        public static void SetOverlayFillPanel(this Image image)
        {
            image.type = Image.Type.Sliced;
            image.fillCenter = true;
            image.pixelsPerUnitMultiplier = 1;
            image.color = new Color(0, 0, 0, 1 / 256f);

            image.rectTransform.anchorMin = Vector2.zero;
            image.rectTransform.anchorMax = Vector2.one;
            image.rectTransform.sizeDelta = Vector2.zero;
            image.rectTransform.anchoredPosition = Vector2.zero;
        }

        /// <summary>
        /// Simple function to find a gameobject with a given name among children
        /// </summary>
        /// <param name="v"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static GameObject FindInChildren(this View v, string name)
        {
            for (int i = 0; i < v.transform.childCount; i++)
            {
                var c = v.transform.GetChild(i);
                
                if (c.name == name)
                {
                    return c.gameObject;
                }
            }

            return null;
        }
    }
}