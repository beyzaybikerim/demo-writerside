﻿using MatchinghamGames.Handyman.Editor;
using MatchinghamGames.StockholmCore.Runtime.Helpers;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MatchinghamGames.Stockholm.Editor.EditorWindows;
using UnityEngine;

namespace MatchinghamGames.StockholmCore.Editor.Models
{
    [CreateAssetMenu(fileName = "StockholmConfig", menuName = "UI Manager/Config")]
    public class StockholmConfig : ScriptableObject
    {
        private static StockholmConfig instance;

        public static StockholmConfig Instance
        {
            get
            {
                if (instance) return instance;

                var fetchFinished = false;
                var fetchStarted = false;

                while (!fetchFinished)
                {
                    if (fetchStarted)
                    {
                        continue;
                    }

                    fetchStarted = true;

                    MGEditorUtility.FetchConfigAndDo<StockholmConfig>(stockholmConfig =>
                    {
                        instance = stockholmConfig;
                        fetchFinished = true;
                    });
                }

                return instance;
            }
        }

        [InfoBox("These namespaces are used in script creation.")] [BoxGroup("Name Spaces")] [LabelWidth(200)]
        public string modelNameSpace = "UI.Models";

        [BoxGroup("Name Spaces")] [LabelWidth(200)]
        public string viewNameSpace = "UI.Views";

        [BoxGroup("Name Spaces")] [LabelWidth(200)]
        public string controllerNameSpace = "UI.Controllers";

        [InfoBox("These suffixes are used for the names " +
                 "in creation of the scripts through " +
                 ConfigEditor.CreateControllerMenuPath
                 + " menu.")]
        [BoxGroup("Class Suffixes")]
        [LabelWidth(200)]
        public string modelClassSuffix = "Model";

        [BoxGroup("Class Suffixes")] [LabelWidth(200)]
        public string viewClassSuffix = "View";

        [BoxGroup("Class Suffixes")] [LabelWidth(200)]
        public string controllerClassSuffix = "Controller";

        [InfoBox("This is used for the name " +
                 "in creation of the view through " +
                 ConfigEditor.CreateViewMenuPath
                 + " menu.")]
        [BoxGroup("Ui Suffixes")]
        //[LabelWidth(200)]
        //public string userControlUiSuffix = "Ui";
        [BoxGroup("Ui Suffixes")]
        [LabelWidth(200)]
        public string subViewSuffix = "View";

        [InfoBox("The scripts are created under these directories. " +
                 "We suggest you to update these settings based on your Folder Hierarchy " +
                 "before creating scripts.")]
        [BoxGroup("Class Paths")]
        [LabelWidth(200)]
        public string modelsPath = "Assets/Scripts/UI/Models";

        [BoxGroup("Class Paths")] [LabelWidth(200)]
        public string viewsPath = "Assets/Scripts/UI/Views";

        [BoxGroup("Class Paths")] [LabelWidth(200)]
        public string controllersPath = "Assets/Scripts/UI/Controllers";

        // [BoxGroup("Resource Paths")] [LabelWidth(200)]
        // public string defaultResourceFolder = ResourcesUiLoader.DEFAULT_RESOURCES_PATH;

        [InfoBox("The sample animation is created on this path." +
                 " Use " + ConfigEditor.CreateAnimationMenuPath + " menu to create one.")]
        [BoxGroup("Utility")]
        [LabelWidth(200)]
        public string uiAnimationsPath = "Assets/Art/Animations/UI";

        public const string UiAnimationControllerSuffix = "AnimController";

        public static string GetControllerPath(string modelRelativePath)
        {
            return string.Format("{0}/{1}{2}.cs", Instance.controllersPath, modelRelativePath, Instance.controllerClassSuffix);
        }

        public static string GetViewPath(string modelRelativePath)
        {
            return string.Format("{0}/{1}{2}.cs", Instance.viewsPath, modelRelativePath, Instance.viewClassSuffix);
        }

        public static string GetModelPath(string modelRelativePath)
        {
            return string.Format("{0}/{1}{2}.cs", Instance.modelsPath, modelRelativePath, Instance.modelClassSuffix);
        }

        public struct Template
        {
            public const string DEFAULT_FOLDER = "Templates";

            public const string MODEL = DEFAULT_FOLDER + "/Model.tex";
            public const string VIEW = DEFAULT_FOLDER + "/View.tex";
            public const string CONTROLLER = DEFAULT_FOLDER + "/Controller.tex";
        }

        public struct Replaceable
        {
            // Add New Replacable words to use in template here.
            public const string NameTag = "[NAME]";

            public static Dictionary<string, string> GetReplacableSpace()
            {
                FieldInfo[] fieldInfos = typeof(Replaceable).GetFields(BindingFlags.Public |
                                                                       BindingFlags.Static | BindingFlags.FlattenHierarchy);

                List<FieldInfo> constantFields = fieldInfos.Where(fi => fi.IsLiteral && !fi.IsInitOnly).ToList();

                Dictionary<string, string> replacableSpace = new Dictionary<string, string>();

                foreach (var field in constantFields)
                {
                    string conditional = (string)field.GetValue(null);

                    replacableSpace.Add(conditional, "[Error]");
                }

                return replacableSpace;
            }
        }

        // Conditional space for template
        public struct Conditional
        {
            // Add New Define words to use in template here.
            public const string Model = "MODEL";
            public const string ViewController = "VIEWCONTROLLER";
            public const string BaseController = "BASECONTROLLER";

            //public const string SubViews = "SUBVIEWS";
            //public const string SubControllers = "SUBCONTROLLERS";
            //public const string BgLock = "BGLOCK";
            //public const string InputLock = "INPUTLOCK";

            public const string EmptyTemplate = "EMPTY_TEMPLATE";
            public const string AnimatorTemplate = "ANIMATOR_TEMPLATE";
            public const string DoTweenTemplate = "DOTWEEN_TEMPLATE";
            public const string LinearScaleTemplate = "LINEARSCALE_TEMPLATE";
            public const string LinearFadeTemplate = "LINEARFADE_TEMPLATE";
            public const string CurvedScaleTemplate = "CURVEDSCALE_TEMPLATE";
            public const string CurvedFadeTemplate = "CURVEDFADE_TEMPLATE";
            public const string SampleCode = "SAMPLE_CODE";

            public static Dictionary<string, bool> GetConditionalSpace()
            {
                FieldInfo[] fieldInfos = typeof(Conditional).GetFields(BindingFlags.Public |
                                                                       BindingFlags.Static | BindingFlags.FlattenHierarchy);

                List<FieldInfo> constantFields = fieldInfos.Where(fi => fi.IsLiteral && !fi.IsInitOnly).ToList();

                Dictionary<string, bool> conditionalSpace = new Dictionary<string, bool>();

                foreach (var field in constantFields)
                {
                    string conditional = (string)field.GetValue(null);

                    conditionalSpace.Add(conditional, false);
                }

                return conditionalSpace;
            }
        }
    }
}