﻿using MatchinghamGames.Handyman.Editor;
using MatchinghamGames.StockholmCore.Editor.Models;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace MatchinghamGames.StockholmCore.Editor
{
    public static class Utility
    {
        public const string SampleAnimationPath = "Packages/com.matchinghamgames.stockholmcore/Runtime/Samples/Resources/SampleAnimController.controller";

        public static T FindNearestParentComponentInHierarchy<T>(GameObject root) where T : MonoBehaviour
        {
            Transform parentInHierarchy = root.transform;
            T comp = null;

            while (comp == null)
            {
                parentInHierarchy = parentInHierarchy.parent;

                if (parentInHierarchy == null)
                {
                    return null;
                }
                else
                {
                    comp = parentInHierarchy.GetComponent<T>();
                }
            }

            return comp;
        }

        public static bool IsUnderCanvasValidation()
        {
            if (Selection.activeObject == null)
            {
                return false;
            }

            if (AssetDatabase.Contains(Selection.activeObject))
            {
                return false;
            }

            Transform parentInHierarchy = Selection.activeGameObject.transform;

            while (parentInHierarchy.GetComponent<Canvas>() == null)
            {
                parentInHierarchy = parentInHierarchy.parent;

                if (parentInHierarchy == null)
                {
                    return false;
                }
            }

            return true;
        }

        public static IEnumerable GetAllControllerObjects()
        {
            var controllers = UnityEditor.AssetDatabase.FindAssets(StockholmConfig.Instance.controllerClassSuffix + " t:MonoScript",
                                                                   new string[] {
                                                                   StockholmConfig.Instance.controllersPath
                                                                   });

            foreach (var monoScript in controllers)
            {
                var assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(monoScript);
                var asset = UnityEditor.AssetDatabase.LoadAssetAtPath<MonoScript>(assetPath);

                if (assetPath.EndsWith(StockholmConfig.Instance.controllerClassSuffix + ".cs"))
                {
                    yield return new ValueDropdownItem(assetPath, asset);
                }
            }
        }

        public static IEnumerable GetAllViewObjects()
        {
            var views = UnityEditor.AssetDatabase.FindAssets(StockholmConfig.Instance.viewClassSuffix + " t:MonoScript",
                                                             new string[] {
                                                             StockholmConfig.Instance.viewsPath,
                                                             "Packages/com.matchinghamgames.stockholmcore/RunTime/Templates/View"
                                                             });

            foreach (var monoScript in views)
            {
                var assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(monoScript);
                var asset = UnityEditor.AssetDatabase.LoadAssetAtPath<MonoScript>(assetPath);

                if (assetPath.EndsWith(StockholmConfig.Instance.viewClassSuffix + ".cs"))
                {
                    yield return new ValueDropdownItem(assetPath, asset);
                }
            }
        }

        public static IEnumerable GetAllModelObjects()
        {
            var models = UnityEditor.AssetDatabase.FindAssets(StockholmConfig.Instance.modelClassSuffix + " t:MonoScript",
                                                              new string[] {
                                                              StockholmConfig.Instance.modelsPath
                                                              });

            foreach (var monoScript in models)
            {
                var assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(monoScript);
                var asset = UnityEditor.AssetDatabase.LoadAssetAtPath<MonoScript>(assetPath);

                if (assetPath.EndsWith(StockholmConfig.Instance.modelClassSuffix + ".cs"))
                {
                    yield return new ValueDropdownItem(assetPath, asset);
                }
            }
        }
    }
}
