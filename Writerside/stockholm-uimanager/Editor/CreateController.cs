﻿using MatchinghamGames.Handyman.Editor;
using MatchinghamGames.Handyman.Editor.Extensions;
using MatchinghamGames.StockholmCore.Editor;
using MatchinghamGames.StockholmCore.Editor.Models;
using MatchinghamGames.StockholmCore.Runtime;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using MatchinghamGames.Stockholm.Editor.EditorWindows;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.UI;

namespace MatchinghamGames.StockholmCore.Editor
{
    public class CreateController : OdinEditorWindow
    {
        private static string[] TEMPLATES = new string[]
        {
            "DoTween",
            "Scaled (Linear)",
            "Scaled (AnimationCurve)",
            "Faded (Linear)",
            "Faded (AnimationCurve)",
            "Animator",
            "Empty"
        };

        protected Dictionary<string, bool> conditionalSpace;
        protected Dictionary<string, string> replacableSpace;

        // [LabelWidth(150)]
        // [EnableIf("@!" + nameof(createController))]
        // [ValueDropdown(nameof(GetAllControllerObjects), IsUniqueList = true)]
        // [InfoBox("Controller component is empty.", InfoMessageType.Warning, "@this.controllerScript == null")]
        // [OnValueChanged(nameof(OnControllerChange))]
        // public MonoScript controllerScript = null;

        [LabelWidth(150)] [EnableIf("@false")] public string viewTypeName = null;

        private Type viewType;

        [LabelWidth(150)] [EnableIf("@false")] public string modelTypeName = null;

        private Type modelType;

        [InfoBox("$" + nameof(modelNameMessage),
            InfoMessageType.Error,
            "@!String.IsNullOrEmpty(" + nameof(modelNameMessage) + ")")]
        [LabelWidth(150)]
        [ShowIf(nameof(createController))]
        [OnValueChanged(nameof(OnModelNameChange))]
        public string modelFullPath = "SampleDir/Sample";

        private string modelFileName = "";
        private string modelUiName = "";

        [LabelWidth(150)] [HorizontalGroup("Set Controller Group")] [HideIf("@true")]
        // [OnValueChanged(nameof(OnCreateControllerToggleChange))]
        public bool createController = true;

        [LabelWidth(150)] [HorizontalGroup("Set View Group")] [ShowIf(nameof(createController))] [OnValueChanged(nameof(OnCreateViewToggleChange))]
        public bool createView = false;

        [LabelWidth(150)] [HorizontalGroup("Set Model Group")] [ShowIf("@" + nameof(createController) + " && " + nameof(createView))]
        public bool createModel = false;

        [LabelWidth(150)] [ShowIf(nameof(createController))] [OnValueChanged(nameof(OnCreateViewToggleChange))]
        public bool includeSampleCode = true;

        //[HorizontalGroup("Set View Group")]
        //[ShowIf(nameof(attachView))]
        //public bool createNewView = true;

        //[HorizontalGroup("Set Model Group")]
        //[ShowIf("@this." + nameof(attachView) + " && this." + nameof(attachModel))]
        //public bool createNewModel = true;

        [LabelWidth(150)] [ShowIf(nameof(createView))] [ValueDropdown(nameof(TEMPLATES))] [BoxGroup("Create View Settings")]
        public string viewTemplate = TEMPLATES[0];

        private string createUiLabelText = "Create UI";

        private string NewControllerPath
        {
            get
            {
                return StockholmConfig.Instance.controllersPath + "/" +
                       modelFullPath +
                       StockholmConfig.Instance.controllerClassSuffix + ".cs";
            }
        }
#pragma warning disable 0414
        private string modelNameMessage;
#pragma warning restore 0414

        // private string CreateUIMessage
        // {
        //     get
        //     {
        //         if (!CreateUiValidation(out string message))
        //         {
        //             return message;
        //         }
        //
        //         return null;
        //     }
        // }

        private string CreateScriptMessage
        {
            get
            {
                string message = "Scripts to be created:";
                bool showMessage = false;

                if (createController)
                {
                    message += "\n- " +
                               StockholmConfig.Instance.controllersPath +
                               "/" +
                               modelFullPath +
                               StockholmConfig.Instance.controllerClassSuffix + ".cs";
                    showMessage = true;
                }

                if (createView)
                {
                    message += "\n- " +
                               StockholmConfig.Instance.viewsPath +
                               "/" +
                               modelFullPath +
                               StockholmConfig.Instance.viewClassSuffix + ".cs";
                    showMessage = true;
                }

                if (createView && createModel)
                {
                    message += "\n- " +
                               StockholmConfig.Instance.modelsPath +
                               "/" +
                               modelFullPath +
                               StockholmConfig.Instance.modelClassSuffix + ".cs";
                    showMessage = true;
                }

                if (!showMessage)
                {
                    return null;
                }

                return message;
            }
        }

        // [MenuItem("GameObject/" + ConfigEditor.CreateControllerHierarchyMenuPath)]
        public static void OpenWindow()
        {
            EditorWindow window = GetWindow<CreateController>();
            window.minSize = new Vector2(480f, 480f);

            window.Show();
        }

        // [MenuItem("GameObject/" + ConfigEditor.CreateControllerHierarchyMenuPath, true)]
        static bool OpenWindowValidation()
        {
            return Utility.IsUnderCanvasValidation();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            conditionalSpace = StockholmConfig.Conditional.GetConditionalSpace();
            replacableSpace = StockholmConfig.Replaceable.GetReplacableSpace();
        }

        // private void UpdateCreateUiButtonLabel()
        // {
        //     modelUiName = "UI";
        //     modelFileName = "";
        //
        //     createUiLabelText = "Create " + modelUiName;
        //
        //     if (controllerScript == null)
        //     {
        //         return;
        //     }
        //
        //     string controllerName = controllerScript.name;
        //     modelFileName = controllerName;
        //
        //     if (controllerName.EndsWith(StockholmConfig.Instance.controllerClassSuffix))
        //     {
        //         modelFileName =
        //             controllerName.Remove(controllerName.Length -
        //                                   StockholmConfig.Instance.controllerClassSuffix.Length);
        //     }
        //
        //     // This is important, we make sure the ui has the same name with the controller,
        //     // so that "Stockholm.Mount" gets the name by address.
        //     modelUiName = modelFileName + StockholmConfig.Instance.controllerClassSuffix;
        //
        //     createUiLabelText = "Create " + modelUiName + " UI";
        // }

        // private void OnControllerChange()
        // {
        //     viewType = null;
        //     viewTypeName = null;
        //
        //     modelType = null;
        //     modelTypeName = null;
        //
        //     UpdateCreateUiButtonLabel();
        //
        //     if (controllerScript == null)
        //     {
        //         return;
        //     }
        //
        //     var controllerType = controllerScript.GetClass();
        //
        //     if (controllerType == null)
        //     {
        //         Debug.Log(
        //             "Could not autocomplete view and model paths, this is caused by assetdatabase refreshing for too long, try reselecting controller script from the dropdown.");
        //         return;
        //     }
        //
        //     var viewProperty = controllerType.GetProperty("View",
        //         BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        //     var modelProperty = controllerType.GetProperty("Model",
        //         BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        //
        //     if (viewProperty != null)
        //     {
        //         viewType = viewProperty.PropertyType;
        //         viewTypeName = viewType.FullName;
        //     }
        //
        //     if (modelProperty != null)
        //     {
        //         modelType = modelProperty.PropertyType;
        //         modelTypeName = modelType.FullName;
        //     }
        // }

        // private void OnCreateControllerToggleChange()
        // {
        //     if (!createController)
        //     {
        //         createView = false;
        //         createModel = false;
        //     }
        //     else
        //     {
        //         controllerScript = null;
        //         createView = true;
        //         createModel = true;
        //         OnControllerChange();
        //     }
        // }

        private void OnCreateViewToggleChange()
        {
            if (!createView)
            {
                createModel = false;
            }
        }

        private void OnModelNameChange()
        {
            if (ModelNameValidation())
            {
                modelNameMessage = null;
            }
            else
            {
                modelNameMessage =
                    "Script name should be composed of letters, numbers and '/'\nExample model name: SampleDir/Sample";
            }
        }

        private bool ModelNameValidation()
        {
            string[] modelFileNameParts = modelFullPath.Split(new char[] { '/' });
            bool isValid = true;
            modelFileName = modelFileNameParts[modelFileNameParts.Length - 1];

            foreach (var namePart in modelFileNameParts)
            {
                if (string.IsNullOrEmpty(namePart))
                {
                    isValid = false;
                    break;
                }
                else if (!Regex.IsMatch(namePart, @"^[a-zA-Z0-9]+$", RegexOptions.IgnoreCase))
                {
                    isValid = false;
                    break;
                }
            }

            return isValid;
        }

        [InfoBox("$" + nameof(CreateScriptMessage),
            InfoMessageType.Info,
            "@!String.IsNullOrEmpty(" + nameof(CreateScriptMessage) + ")")]
        [ShowIf(nameof(createController))]
        [Button(ButtonSizes.Large)]
        public void CreateScripts()
        {
            if (!ModelNameValidation())
            {
                return;
            }

            conditionalSpace[StockholmConfig.Conditional.ViewController] = createView && !createModel;
            conditionalSpace[StockholmConfig.Conditional.BaseController] = !createView;
            conditionalSpace[StockholmConfig.Conditional.Model] = createModel;
            conditionalSpace[StockholmConfig.Conditional.SampleCode] = includeSampleCode;

            conditionalSpace[StockholmConfig.Conditional.DoTweenTemplate] = TEMPLATES[0] == viewTemplate;
            conditionalSpace[StockholmConfig.Conditional.LinearScaleTemplate] = TEMPLATES[1] == viewTemplate;
            conditionalSpace[StockholmConfig.Conditional.CurvedScaleTemplate] = TEMPLATES[2] == viewTemplate;
            conditionalSpace[StockholmConfig.Conditional.LinearFadeTemplate] = TEMPLATES[3] == viewTemplate;
            conditionalSpace[StockholmConfig.Conditional.CurvedFadeTemplate] = TEMPLATES[4] == viewTemplate;
            conditionalSpace[StockholmConfig.Conditional.AnimatorTemplate] = TEMPLATES[5] == viewTemplate;
            conditionalSpace[StockholmConfig.Conditional.EmptyTemplate] = TEMPLATES[6] == viewTemplate;

            replacableSpace[StockholmConfig.Replaceable.NameTag] = modelFileName;

            if (CreateComponentsValidation())
            {
                CreateComponentsFromTemplate();

                EditorUtility.SetDirty(StockholmConfig.Instance);
                AssetDatabase.Refresh();

                // controllerScript = (MonoScript) AssetDatabase.LoadAssetAtPath(NewControllerPath, typeof(MonoScript));
                // OnControllerChange();
            }
        }

        bool CreateComponentsValidation()
        {
            List<string> overwrittenFiles = new List<string>();

            if (createModel)
            {
                string modelPath = StockholmConfig.GetModelPath(this.modelFullPath);

                if (!string.IsNullOrEmpty(AssetDatabase.AssetPathToGUID(modelPath)))
                {
                    overwrittenFiles.Add(modelPath);
                }
            }

            if (createView)
            {
                string viewPath = StockholmConfig.GetViewPath(this.modelFullPath);

                if (!string.IsNullOrEmpty(AssetDatabase.AssetPathToGUID(viewPath)))
                {
                    overwrittenFiles.Add(viewPath);
                }
            }

            if (!string.IsNullOrEmpty(AssetDatabase.AssetPathToGUID(NewControllerPath)))
            {
                overwrittenFiles.Add(NewControllerPath);
            }

            if (overwrittenFiles.Count > 0)
            {
                string message = "The following files will be overwritten;";

                foreach (var path in overwrittenFiles)
                {
                    message += $"\n- {path}";
                }

                message += "\nDo you want to continue?";

                if (!EditorUtility.DisplayDialog("Conflict with previously created files",
                        message,
                        "Overwrite",
                        "Cancel"))
                {
                    return false;
                }
            }

            return true;
        }

        void CreateComponentsFromTemplate()
        {
            if (createModel)
            {
                string modelPath = StockholmConfig.GetModelPath(this.modelFullPath);
                CreateComponentFromTemplate(StockholmConfig.Template.MODEL, modelPath, conditionalSpace,
                    replacableSpace);
            }

            if (createView)
            {
                string viewPath = StockholmConfig.GetViewPath(this.modelFullPath);
                CreateComponentFromTemplate(StockholmConfig.Template.VIEW, viewPath, conditionalSpace, replacableSpace);
            }

            CreateComponentFromTemplate(StockholmConfig.Template.CONTROLLER, NewControllerPath, conditionalSpace,
                replacableSpace);

            // createController = false;
            // OnCreateControllerToggleChange();
        }

        protected void CreateComponentFromTemplate(string relativeTemplatePath,
            string scriptPath,
            Dictionary<string, bool> conditionalSettings,
            Dictionary<string, string> replacableSettings)
        {
            string script =
                this.GenerateCodeFromTemplate(relativeTemplatePath, conditionalSettings, replacableSettings);

            if (!string.IsNullOrEmpty(script))
            {
                string folderPath = Path.GetDirectoryName(scriptPath);
                Directory.CreateDirectory(folderPath);
                File.WriteAllText(scriptPath, script);
            }
        }
    }
}