﻿#if ODIN_INSPECTOR
using System.Linq;
using MatchinghamGames.Handyman.Editor;
using MatchinghamGames.StockholmCore.Editor;
using MatchinghamGames.StockholmCore.Editor.Abstract;
using MatchinghamGames.StockholmCore.Editor.Models;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace MatchinghamGames.Stockholm.Editor.EditorWindows
{
    public class ConfigEditor : OdinEditorWindow
    {
        public const string MenuPath = "Matchingham/Stockholm/";
        public const string CreateControllerMenuPath = MenuPath + "Create Controller";
        public const string CreateViewMenuPath = MenuPath + "Create View";
        public const string CreateAnimationMenuPath = MenuPath + "Create View Animator";

        [MenuItem(MenuPath + "Config")]
        private static void OpenUiConfig()
        {
            MGEditorUtility.FetchConfigAndDo<StockholmConfig>(config => Selection.activeObject = config);
        }

        [MenuItem(CreateControllerMenuPath)]
        private static void OpenCreateControllerWindow()
        {
            CreateController.OpenWindow();
        }

        [MenuItem(CreateViewMenuPath)]
        private static void OpenCreateViewWindow()
        {
            CreateView.OpenCreateViewWindow();
        }

        [MenuItem(CreateAnimationMenuPath)]
        private static void OpenCreateViewAnimationWindow()
        {
            CreateSampleAnimation.OpenWindow();
        }
    }
}
#endif