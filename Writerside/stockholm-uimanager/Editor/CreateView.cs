﻿using MatchinghamGames.Handyman.Editor;
using MatchinghamGames.Handyman.Editor.Extensions;
using MatchinghamGames.StockholmCore.Editor;
using MatchinghamGames.StockholmCore.Editor.Models;
using MatchinghamGames.StockholmCore.Runtime;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using MatchinghamGames.Stockholm.Editor.EditorWindows;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.UI;

namespace MatchinghamGames.StockholmCore.Editor.Abstract
{
    public class CreateView : OdinEditorWindow
    {
        private readonly static string[] TEMPLATES = new string[]
        {
            "DoTween",
            "Scaled (Linear)",
            "Scaled (AnimationCurve)",
            "Faded (Linear)",
            "Faded (AnimationCurve)",
            "Animator",
            "Empty"
        };

        protected Dictionary<string, bool> conditionalSpace;
        protected Dictionary<string, string> replacableSpace;

        // [LabelWidth(150)]
        // [EnableIf("@!" + nameof(createView))]
        // [ValueDropdown(nameof(GetAllViewObjects), IsUniqueList = true)]
        // [InfoBox("View component is empty.", InfoMessageType.Warning, "@this.viewScript == null")]
        // [OnValueChanged(nameof(OnViewChange))]
        // public MonoScript viewScript = null;

        [LabelWidth(150)] [EnableIf("@false")] public string modelTypeName = null;

        private Type modelType;

        [LabelWidth(150)] [ShowIf(nameof(createView))]
        public string newModelName = "SampleDir/SampleFile";

        [LabelWidth(150)]
        [HorizontalGroup("Set View Group")]
        // [OnValueChanged(nameof(OnCreateViewToggleChange))]
        [HideIf("@true")]
        public bool createView = true;

        [LabelWidth(150)] [HorizontalGroup("Set Model Group")] [ShowIf(nameof(createView))]
        public bool createModel = false;

        [LabelWidth(150)] [ShowIf(nameof(createView))]
        public bool includeSampleCode = true;

        [LabelWidth(150)] [ShowIf(nameof(createView))] [ValueDropdown(nameof(TEMPLATES))] [BoxGroup("Create View Settings")]
        public string viewTemplate = TEMPLATES[0];

        private string NewViewPath
        {
            get
            {
                return StockholmConfig.Instance.viewsPath + "/" +
                       newModelName +
                       StockholmConfig.Instance.subViewSuffix + ".cs";
            }
        }

        private string CreateScriptMessage
        {
            get
            {
                string message = "Scripts to be created:";
                bool showMessage = false;

                if (createView)
                {
                    message += "\n- " +
                               StockholmConfig.Instance.viewsPath +
                               "/" +
                               newModelName +
                               StockholmConfig.Instance.viewClassSuffix + ".cs";
                    showMessage = true;
                }

                if (createView && createModel)
                {
                    message += "\n- " +
                               StockholmConfig.Instance.modelsPath +
                               "/" +
                               newModelName +
                               StockholmConfig.Instance.modelClassSuffix + ".cs";
                    showMessage = true;
                }

                if (!showMessage)
                {
                    return null;
                }

                return message;
            }
        }

        /// <summary>
        /// Create Animated Image
        /// </summary>
        // [MenuItem("GameObject/" + ConfigEditor.CreateViewHierarchyMenuPath)]
        public static void OpenCreateViewWindow()
        {
            EditorWindow window = GetWindow<CreateView>();
            window.minSize = new Vector2(480f, 480f);

            window.Show();
        }

        // [MenuItem("GameObject/" + ConfigEditor.CreateViewHierarchyMenuPath, true)]
        private static bool OpenCreateViewWindowValidation()
        {
            return Utility.IsUnderCanvasValidation();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            conditionalSpace = StockholmConfig.Conditional.GetConditionalSpace();
            replacableSpace = StockholmConfig.Replaceable.GetReplacableSpace();
        }

        private void OnViewChange()
        {
        }

        [InfoBox("$" + nameof(CreateScriptMessage),
            InfoMessageType.Info,
            "@!String.IsNullOrEmpty(" + nameof(CreateScriptMessage) + ")")]
        [ShowIf(nameof(createView))]
        [Button(ButtonSizes.Large)]
        public void CreateScripts()
        {
            string modelName = "";

            if (string.IsNullOrEmpty(newModelName))
            {
                Debug.LogError("Script name cannot be empty");
                return;
            }
            else
            {
                string[] modelFileNameParts = newModelName.Split(new char[] { '/' });
                bool error = false;
                modelName = modelFileNameParts[modelFileNameParts.Length - 1];

                foreach (var namePart in modelFileNameParts)
                {
                    if (string.IsNullOrEmpty(namePart))
                    {
                        error = true;
                        break;
                    }
                    else if (!Regex.IsMatch(namePart, @"^[a-zA-Z0-9]+$", RegexOptions.IgnoreCase))
                    {
                        error = true;
                        break;
                    }
                }

                if (error)
                {
                    Debug.LogError(
                        "Script name should be composed of letters and '/'\nExample model name: SampleDir/Sample");
                    return;
                }
            }

            conditionalSpace[StockholmConfig.Conditional.Model] = createModel;
            conditionalSpace[StockholmConfig.Conditional.SampleCode] = includeSampleCode;

            conditionalSpace[StockholmConfig.Conditional.DoTweenTemplate] = TEMPLATES[0] == viewTemplate;
            conditionalSpace[StockholmConfig.Conditional.LinearScaleTemplate] = TEMPLATES[1] == viewTemplate;
            conditionalSpace[StockholmConfig.Conditional.CurvedScaleTemplate] = TEMPLATES[2] == viewTemplate;
            conditionalSpace[StockholmConfig.Conditional.LinearFadeTemplate] = TEMPLATES[3] == viewTemplate;
            conditionalSpace[StockholmConfig.Conditional.CurvedFadeTemplate] = TEMPLATES[4] == viewTemplate;
            conditionalSpace[StockholmConfig.Conditional.AnimatorTemplate] = TEMPLATES[5] == viewTemplate;
            conditionalSpace[StockholmConfig.Conditional.EmptyTemplate] = TEMPLATES[6] == viewTemplate;

            replacableSpace[StockholmConfig.Replaceable.NameTag] = modelName;

            if (CreateComponentsValidation())
            {
                CreateComponentsFromTemplate();

                EditorUtility.SetDirty(StockholmConfig.Instance);
                AssetDatabase.Refresh();

                // viewScript = (MonoScript) AssetDatabase.LoadAssetAtPath(NewViewPath, typeof(MonoScript));
                OnViewChange();
            }
        }

        bool CreateComponentsValidation()
        {
            List<string> overwrittenFiles = new List<string>();

            if (createModel)
            {
                string modelPath = StockholmConfig.GetModelPath(newModelName);

                if (!string.IsNullOrEmpty(AssetDatabase.AssetPathToGUID(modelPath)))
                {
                    overwrittenFiles.Add(modelPath);
                }
            }

            if (!string.IsNullOrEmpty(AssetDatabase.AssetPathToGUID(NewViewPath)))
            {
                overwrittenFiles.Add(NewViewPath);
            }

            if (overwrittenFiles.Count > 0)
            {
                string message = "The following files will be overwritten;";

                foreach (var path in overwrittenFiles)
                {
                    message += $"\n- {path}";
                }

                message += "\nDo you want to continue?";

                if (!EditorUtility.DisplayDialog("Conflict with previously created files",
                        message,
                        "Overwrite",
                        "Cancel"))
                {
                    return false;
                }
            }

            return true;
        }

        void CreateComponentsFromTemplate()
        {
            if (createModel)
            {
                string modelPath = StockholmConfig.Instance.modelsPath + "/" +
                                   newModelName +
                                   StockholmConfig.Instance.modelClassSuffix + ".cs";
                CreateComponentFromTemplate(StockholmConfig.Template.MODEL, modelPath, conditionalSpace,
                    replacableSpace);
            }

            CreateComponentFromTemplate(StockholmConfig.Template.VIEW, NewViewPath, conditionalSpace, replacableSpace);

            // createView = false;
            // OnCreateViewToggleChange();
        }

        protected void CreateComponentFromTemplate(string relativeTemplatePath,
            string scriptPath,
            Dictionary<string, bool> conditionalSettings,
            Dictionary<string, string> replacableSettings)
        {
            string script =
                this.GenerateCodeFromTemplate(relativeTemplatePath, conditionalSettings, replacableSettings);

            if (!string.IsNullOrEmpty(script))
            {
                string folderPath = Path.GetDirectoryName(scriptPath);
                Directory.CreateDirectory(folderPath);
                File.WriteAllText(scriptPath, script);
            }
        }
    }
}