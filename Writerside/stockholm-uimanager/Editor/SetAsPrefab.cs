﻿// This script creates a new menu item Examples>Create Prefab in the main menu.
// Use it to create Prefab(s) from the selected GameObject(s).
// It will be placed in the root Assets folder.

using System.IO;
using MatchinghamGames.Handyman.Editor;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector.Editor;
using MatchinghamGames.StockholmCore.Runtime;
using MatchinghamGames.StockholmCore.Editor.Models;

namespace MatchinghamGames.StockholmCore.Editor
{
    public class CreatePrefabMenu : OdinEditorWindow
    {
        // [MenuItem("GameObject/UI/MG Stockholm/Save As Prefab")]
        // static void CreatePrefab()
        // {
        //     // Set the path as within the Assets folder,
        //     // and name it as the GameObject's name with the .Prefab format
        //     string localPath = $"{StockholmConfig.Instance.defaultResourceFolder}/{Selection.activeGameObject.name}.prefab";
        //
        //     if (!string.IsNullOrEmpty(AssetDatabase.AssetPathToGUID(localPath)))
        //     {
        //         if (!EditorUtility.DisplayDialog("Conflict with previously created assets",
        //                                          "Are you sure you want to replace " +
        //                                          Selection.activeGameObject.name +
        //                                          ".prefab in " +
        //                                          StockholmConfig.Instance.defaultResourceFolder, 
        //                                          "Overwrite", 
        //                                          "Cancel"))
        //         {
        //             return;
        //         }
        //     }
        //     
        //     MGEditorUtility.CreateFolder(Path.GetDirectoryName(localPath));
        //
        //     // Create the new Prefab.
        //     PrefabUtility.SaveAsPrefabAssetAndConnect(Selection.activeGameObject, localPath, InteractionMode.UserAction);
        // }

        // Disable the menu item if no selection is in place.
        [MenuItem("GameObject/UI/MG Stockholm/Save As Prefab", true)]
        static bool ValidateCreatePrefab()
        {
            return Selection.activeGameObject != null &&
                   !EditorUtility.IsPersistent(Selection.activeGameObject) &&
                   Selection.gameObjects.Length == 1 &&
                   Selection.activeGameObject.GetComponent<Controller>() != null;
        }
    }
}