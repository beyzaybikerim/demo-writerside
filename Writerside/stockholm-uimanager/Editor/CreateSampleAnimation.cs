using MatchinghamGames.Handyman.Editor;
using MatchinghamGames.Handyman.Editor.Extensions;
using MatchinghamGames.StockholmCore.Editor;
using MatchinghamGames.StockholmCore.Editor.Models;
using MatchinghamGames.StockholmCore.Runtime;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Animations;
using UnityEditor.VersionControl;
using UnityEngine;
using UnityEngine.UI;

namespace MatchinghamGames.StockholmCore.Editor
{
    public class CreateSampleAnimation : OdinEditorWindow
    {
        [LabelWidth(150)]
        public string animationControllerName = "SampleDir/Sample";
        
        public string CreateAnimatorMessage
        {
            get
            {
                return $"{NewAnimationControllerPath} is going to be created.";
            }
        }

        private string NewAnimationControllerPath
        {
            get
            {
                return $"{StockholmConfig.Instance.uiAnimationsPath}/{animationControllerName}{StockholmConfig.UiAnimationControllerSuffix}.controller";
            }
        }
        
        [InfoBox("$" + nameof(CreateAnimatorMessage),
            InfoMessageType.Info,
            "@!String.IsNullOrEmpty(" + nameof(CreateAnimatorMessage) + ")")]
        [Button(ButtonSizes.Large)]
        public void CreateAnimator()
        {
            if (!string.IsNullOrEmpty(AssetDatabase.AssetPathToGUID(NewAnimationControllerPath)))
            {
                if (!EditorUtility.DisplayDialog("Conflict with previously created assets",
                    "Are you sure you want to replace " +
                    NewAnimationControllerPath, 
                    "Overwrite", 
                    "Cancel"))
                {
                    return;
                }
            }
            
            MGEditorUtility.CreateFolder(Path.GetDirectoryName(NewAnimationControllerPath));
            AssetDatabase.CopyAsset(Utility.SampleAnimationPath, NewAnimationControllerPath);
            AssetDatabase.Refresh();
        }
        
        // [MenuItem("Assets/Create/Matchingham/MG Stockholm/Create View Animation")]
        public static void OpenWindow()
        {
            EditorWindow window = GetWindow<CreateSampleAnimation>();
            window.maxSize = new Vector2(480f, 100f);

            window.Show();
        }
    }
}
