# Changelog

## [3.0.0] - 2023-12-07
### Changed
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.0.1 -> v5.0.0

## [2.2.0] - 2023-12-07
### Added
* RemoveHandlers for controllers
* Loading by ui name is now possible
### Removed 
* Reverted log4net dependency, go to v3.0.0 for log4net
* Handyman dependency: v5.0.0 -> v4.0.1
* Minimum Unity version reverted back to 2020.3
### Fixed
* Obsolete documentation Update, created Demo under Samples
* Root, bgOverlay, lockOverlay scaling wrong bug

## [2.1.0] - 2023-11-17
### Added
* Unity 2023.2.x compatibility (see `Migration Guide`)
### Changed
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.0.1 -> v5.0.0
### Removed
* TextMeshPro dependency (see `Migration Guide`)
### Migration Guide
* TextMeshPro dependency:
    * Unity 2023.2 or newer users: TextMeshPro package is deprecated. Unity UI v2+ package includes TextMeshPro. It is necessary to remove TextMeshPro package.
    * Unity 2023.1 and older users: TextMeshPro package is no longer a dependency, but it is necessary to install to use this package properly.

## [2.0.10] - 2023-09-18
### Added
* Dispose NullReference checks

## [2.0.9] - 2023-09-04
### Fixed
* Unmount, collection modified bug fix.

## [2.0.8] - 2023-03-02
### Fixed
* Mec Coroutines object destroy error fix.

## [2.0.7] - 2023-03-01
### Changed
* More effective coroutines choice for LerpView.

## [2.0.6] - 2023-02-13
### Changed
* BugFix
* ViewModel Binding Error.

## [2.0.5] - 2023-02-07
### Changed
* Unmount on CloseEnd.

## [2.0.4] - 2023-02-05
### Fixed
* Model access case before initialization.

## [2.0.2] - 2022-08-10
### Fixed
* Added Standalone View Samples ready to use.
* Background and Foreground animations can be added with View now.

## [2.0.0] - 2022-08-07
### Changed
* MVC logic has been changed.

## [1.0.20] - 2022-07-25
### Changed
* Singleton logic changed, FetchRoot needed.

## [1.0.19] - 2022-03-09
### Changed
* Namespace correction on asmdef files
* Handyman update

## [1.0.18] - 2021-02-17
### Changed
* OnBinding and OnUnbinding events.
* UI Layer Fix.
* TextMeshGuiPro addition.

## [1.0.15] - 2021-12-08
### Changed
* Double Binding Fix.

## [1.0.14] - 2021-12-08
### Changed
* Trying to open and close without waiting for animation to complete caused complications. Now we can just immediately change the state without waiting.



